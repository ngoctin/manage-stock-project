const socket = require('socket.io');

const socketIOConfig = server =>
  /* eslint-disable-next-line no-unused-vars */
  new Promise((resolve, reject) => {
    const io = socket(server);
    resolve(io);
  });

module.exports = {
  socketIOConfig,
};
