require('dotenv').config();

const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const morgan = require('morgan');
const cookieParser = require('cookie-parser');
const expHbs = require('express-handlebars');
const cors = require('cors');
const sectionHbs = require('express-handlebars-sections');
const numeral = require('numeral');
const cookieSession = require('cookie-session');

const app = express();
const http = require('http').createServer(app);

const Handlebars = require('handlebars');
const HandlebarsIntl = require('handlebars-intl');

HandlebarsIntl.registerWith(Handlebars);

const router = require('../routes');
const routerApi = require('../routes/api');
const socket = require('../app/notification-system/notification-system');
const registerFuncHbsHelper = require('../lib/registerFuncHbsHelper');

const isProduction = process.env.NODE_ENV === 'production';

module.exports = () =>
  /* eslint-disable-next-line */
  new Promise(async (resolve, reject) => {
    try {
      /* initialize socket */
      await socket(http);

      app.engine(
        '.hbs',
        expHbs({
          defaultLayout: path.resolve(
            __dirname,
            '../app/views/shared/layouts/admin/_layout_admin.hbs'
          ),
          extname: 'hbs',
          partialsDir: path.resolve(__dirname, '../app/views/shared/partials'),
          helpers: {
            section: sectionHbs(),
            format: val => numeral(+val).format('0,0'),
            strcat: (str1, str2) => `${str1} ${str2}`,
            tax: (price, taxPercentage) => (+price * +taxPercentage) / 100,
            plusTax: (price, taxPercentage) =>
              +price + (+price * +taxPercentage) / 100,
            sliceString: registerFuncHbsHelper.sliceString,
            renderAction: registerFuncHbsHelper.renderActionFollowRole,
            mul: (a, b) => +a * +b,
          },
        })
      );

      app.use(cors());

      /* set static file for server */
      app.use(
        '/static',
        express.static(path.join(__dirname, '../app/views/assets'))
      );

      app.set('view engine', 'hbs');

      app.set('views', path.resolve(__dirname, '../app/views'));

      app.use(morgan('dev'));

      /* protect application, prevent attacking XSS and CSRF */
      app.use(helmet());

      /* cookie-parser */
      app.use(cookieParser(process.env.COOKIE_KEY));

      // parse application/x-www-form-urlencoded
      app.use(bodyParser.urlencoded({ extended: false }));

      // parse application/json
      app.use(bodyParser.json());

      app.use(express.json());

      // set session for application
      app.use(
        cookieSession({
          maxAge: 1 * 60 * 60 * 1000,
          keys: [process.env.COOKIE_KEY],
        })
      );

      app.use((req, res, next) => {
        req.session.previousUrl =
          req.session.currentUrl || '/manage-statisticals';
        req.session.currentUrl = req.originalUrl;
        next();
      });

      app.use(routerApi);

      app.use(router);

      /* catch 404 errror */
      /* eslint-disable-next-line no-unused-vars */
      app.use((req, res, next) => {
        const error = new Error('Resource does not exist.');
        error.status = 404;
        next(error);
      });

      /* handle error on production environment */
      if (isProduction) {
        /* eslint-disable-next-line no-unused-vars */
        app.use((error, req, res, next) => {
          return res.status(error.status || 500).json({
            error: {
              message: error.message,
              error,
            },
          });
        });
      }

      /* handle error on development environment */
      /* eslint-disable-next-line consistent-return */
      /* eslint-disable-next-line no-unused-vars */
      app.use((error, req, res, next) => {
        return res.status(error.status || 500).json({
          error: {
            message: error.message,
            error,
          },
        });
      });

      resolve(http);
    } catch (error) {
      reject(error);
    }
  });
