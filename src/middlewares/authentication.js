const userService = require('../app/services/user.service');
const enforcePolicy = require('../authz/enforce');

const globalVariables = require('../lib/globalVariables');

// Do not allow request when has logined
const notRequest = (req, res, next) => {
  try {
    if (req.signedCookies.managestockSession)
      return res.redirect(req.session.returnUrl);

    next();
  } catch (error) {
    return next(error);
  }
};

// Checking that user has logined
const authencated = (req, res, next) => {
  try {
    if (!req.signedCookies.managestockSession)
      return res.redirect('/login-account');

    next();
  } catch (error) {
    return next(error);
  }
};

// Get role of user
const getRoleUser = async (req, res, next) => {
  try {
    const user = await userService.getRoleOfUser(
      req.signedCookies.managestockSession
    );

    if (user.value instanceof Error) throw user.value;

    req.user = user.value;
    res.locals.user = req.user;

    next();
  } catch (error) {
    return next(error);
  }
};

// Render side bar is responsibility for role
const renderSideBar = (req, res, next) => {
  try {
    let items = null; // eslint-disable-line
    let role = req.user.roles; // eslint-disable-line

    if (role.find(rol => rol === 'STAFF')) {
      items = globalVariables.listItemOfStaff;
    } else if (role.find(rol => rol === 'MANAGER')) {
      items = globalVariables.listItemOfManager;
    } else {
      items = globalVariables.listItemOfAdmin;
    }

    items.forEach(e => {
      e.active = '';
    });

    const item = items.find(element => element.router === req.path);

    /* eslint-disable-next-line no-unused-expressions */
    item ? (item.active = 'active') : null;

    /* eslint-disable-next-line */
    res.locals._sidebar = items;

    next();
  } catch (error) {
    return next(error);
  }
};

// Check role of user
const checkRoleOfUser = async (req, res, next) => {
  try {
    const role = req.user.roles;
    const path = req.originalUrl;
    const { method } = req;

    if (!enforcePolicy.enforce({ role, path, method }))
      return res.status(403).json({
        status: 403,
        message: 'USR_04',
      });

    next();
  } catch (error) {
    return next(error);
  }
};

module.exports = {
  authencated,
  getRoleUser,
  notRequest,
  renderSideBar,
  checkRoleOfUser,
};
