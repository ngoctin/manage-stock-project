/*
 Navicat Premium Data Transfer

 Source Server         : mange_stock
 Source Server Type    : MySQL
 Source Server Version : 50728
 Source Host           : 103.137.184.41:3306
 Source Schema         : manage_stock

 Target Server Type    : MySQL
 Target Server Version : 50728
 File Encoding         : 65001

 Date: 13/12/2019 22:59:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id_category` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `category_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id_category`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES ('6f8682df-d01e-495d-925b-0fc88cc1d948', 'fdsfdsfds', 'fdsfdfdfsdf');
INSERT INTO `categories` VALUES ('2249dab2-808a-4027-ae18-6bb1fc5b97d0', 'fsdfsdf', 'fdfsdfsdf');
INSERT INTO `categories` VALUES ('6f8682df-d01e-495d-925b-12fab', 'fsdfsdf', 'fdfsdfsdf');
INSERT INTO `categories` VALUES ('f83f6891-deee-4a68-8a40-4c11764e1c3f', 'fsdfsdf', 'fdfsdfsdf');
INSERT INTO `categories` VALUES ('xxx', 'newname', 'update');
INSERT INTO `categories` VALUES ('1234', 'abc', 'abc');

-- ----------------------------
-- Table structure for customers
-- ----------------------------
DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers`  (
  `id_customer` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `first_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `last_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `phone` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `address` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id_customer`) USING BTREE,
  UNIQUE INDEX `idx_phone`(`phone`) USING BTREE,
  FULLTEXT INDEX `idx_name`(`first_name`, `last_name`)
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for delivery_detail
-- ----------------------------
DROP TABLE IF EXISTS `delivery_detail`;
CREATE TABLE `delivery_detail`  (
  `id_delivery` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `id_delivery_detail` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `quantity_delivery` int(11) NOT NULL DEFAULT 0,
  `price_item` decimal(16, 0) NOT NULL DEFAULT 0,
  `sub_total` decimal(16, 0) NOT NULL,
  `id_product` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id_delivery`, `id_delivery_detail`) USING BTREE,
  INDEX `idx_id_product`(`id_product`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for delivery_votes
-- ----------------------------
DROP TABLE IF EXISTS `delivery_votes`;
CREATE TABLE `delivery_votes`  (
  `id_delivery` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `delivery_date` datetime(0) NOT NULL,
  `total_product` int(11) NOT NULL DEFAULT 0,
  `total_price` decimal(16, 0) NULL DEFAULT 0,
  `id_request` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id_delivery`) USING BTREE,
  INDEX `idx_id_request`(`id_request`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `id_order` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_on` datetime(0) NOT NULL,
  `total_product` int(11) NOT NULL DEFAULT 0,
  `total_price` decimal(16, 0) NOT NULL DEFAULT 0,
  `id_tax` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `id_customer` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `id_user` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id_order`) USING BTREE,
  INDEX `idx_create_on`(`create_on`) USING BTREE,
  INDEX `idx_id_customer`(`id_customer`) USING BTREE,
  INDEX `idx_id_user`(`id_user`) USING BTREE,
  INDEX `idx_id_tax`(`id_tax`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES ('1234', '2019-10-10 23:00:00', 1, 1, '1234', '1234', '1234');

-- ----------------------------
-- Table structure for orders_detail
-- ----------------------------
DROP TABLE IF EXISTS `orders_detail`;
CREATE TABLE `orders_detail`  (
  `id_order` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `id_order_detail` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created_date` datetime(0) NOT NULL,
  `quantity_order` int(11) NOT NULL,
  `unit_cost` decimal(16, 0) NOT NULL,
  `line_total` decimal(16, 0) NOT NULL,
  `id_product` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id_order`, `id_order_detail`) USING BTREE,
  INDEX `idx_id_product`(`id_product`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orders_detail
-- ----------------------------
INSERT INTO `orders_detail` VALUES ('1234', '1234', '2019-10-10 23:37:57', 1, 1, 1, '1234');

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions`  (
  `id_permission` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `permission_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id_permission`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `id_product` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `product_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `descriptions` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(16, 0) NOT NULL,
  `img_1` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `img_2` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `id_supplier` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `id_category` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id_product`) USING BTREE,
  INDEX `idx_id_supplier`(`id_supplier`) USING BTREE,
  INDEX `idx_id_category`(`id_category`) USING BTREE,
  FULLTEXT INDEX `idx_search`(`product_name`, `descriptions`)
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('1234', 'update', 'update', 2, '2', '2', '12345', '12345');

-- ----------------------------
-- Table structure for request_detail
-- ----------------------------
DROP TABLE IF EXISTS `request_detail`;
CREATE TABLE `request_detail`  (
  `id_request` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `id_request_detail` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `add_at` datetime(0) NOT NULL,
  `quantity_request` int(11) NOT NULL,
  `id_product` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id_request`, `id_request_detail`) USING BTREE,
  INDEX `idx_id_product`(`id_product`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for request_votes
-- ----------------------------
DROP TABLE IF EXISTS `request_votes`;
CREATE TABLE `request_votes`  (
  `id_request` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `request_date` datetime(0) NOT NULL,
  `total_product` int(11) NOT NULL DEFAULT 0,
  `id_user` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `id_supplier` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id_request`) USING BTREE,
  INDEX `idx_request_date`(`request_date`) USING BTREE,
  INDEX `idx_id_user`(`id_user`) USING BTREE,
  INDEX `id_supplier`(`id_supplier`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `id_role` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `role_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id_role`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for stock_products
-- ----------------------------
DROP TABLE IF EXISTS `stock_products`;
CREATE TABLE `stock_products`  (
  `id_pro_stk` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `quantity_product` int(11) NOT NULL,
  `id_store` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `id_product` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id_pro_stk`) USING BTREE,
  INDEX `idx_id_stock`(`id_store`) USING BTREE,
  INDEX `idx_id_product`(`id_product`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for stores
-- ----------------------------
DROP TABLE IF EXISTS `stores`;
CREATE TABLE `stores`  (
  `id_store` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `store_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `phone` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `address` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `id_user` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id_store`) USING BTREE,
  UNIQUE INDEX `idx_email`(`email`) USING BTREE,
  INDEX `idx_id_user`(`id_user`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for suppliers
-- ----------------------------
DROP TABLE IF EXISTS `suppliers`;
CREATE TABLE `suppliers`  (
  `id_supplier` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `supplier_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `address` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id_supplier`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of suppliers
-- ----------------------------
INSERT INTO `suppliers` VALUES ('1234', 'giang', '123', 'ho chi minh');

-- ----------------------------
-- Table structure for tax
-- ----------------------------
DROP TABLE IF EXISTS `tax`;
CREATE TABLE `tax`  (
  `id_tax` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `percentage_tax` decimal(5, 2) NOT NULL DEFAULT 0.00,
  `type_tax` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_tax`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user_permission
-- ----------------------------
DROP TABLE IF EXISTS `user_permission`;
CREATE TABLE `user_permission`  (
  `id_permission` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `id_user` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id_permission`, `id_user`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role`  (
  `id_role` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `id_user` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id_role`, `id_user`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id_user` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `display_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `gender` varchar(7) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `address` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_actived` tinyint(1) NOT NULL DEFAULT 0,
  `verified_code` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `id_store` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `id_manager` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id_user`) USING BTREE,
  UNIQUE INDEX `idx_user_email_name`(`user_name`, `email`) USING BTREE,
  FULLTEXT INDEX `idx_search_user`(`user_name`, `email`)
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1234', 'hello', 'hdfdfdello', 'hello@', '12345', 'nam', 'adfasdfds', 1, 'abc', '1234', '1234');

-- ----------------------------
-- Procedure structure for CreateCategory
-- ----------------------------
DROP PROCEDURE IF EXISTS `CreateCategory`;
delimiter ;;
CREATE PROCEDURE `CreateCategory`(IN xid_category VARCHAR(50),
	IN xcategory_name VARCHAR(100),
	IN xdescription VARCHAR(200))
BEGIN
	INSERT INTO categories(`id_category`,`category_name`,`description`)
    VALUES(xid_category,xcategory_name,xdescription);
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for CreateDetailOrder
-- ----------------------------
DROP PROCEDURE IF EXISTS `CreateDetailOrder`;
delimiter ;;
CREATE PROCEDURE `CreateDetailOrder`(IN xid_order VARCHAR(50),
IN xid_order_detail VARCHAR(50),
IN xcreated_date DATETIME,
IN xquantity_order INT,
IN xunit_cost DECIMAL(16, 0),
IN xline_total DECIMAL(16, 0),
IN xid_product VARCHAR(50))
BEGIN
    INSERT INTO orders_detail(`id_order`, `id_order_detail`, `created_date`, `quantity_order`, `unit_cost`, `line_total`, `id_product`)
    VALUES(xid_order, xid_order_detail, xcreated_date, xquantity_order, xunit_cost, xline_total, xid_product);
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for CreateOrder
-- ----------------------------
DROP PROCEDURE IF EXISTS `CreateOrder`;
delimiter ;;
CREATE PROCEDURE `CreateOrder`(IN xid_order VARCHAR(50),
IN xcreate_on DATETIME,
IN xtotal_product INT,
IN xtotal_price DECIMAL(16, 0),
IN xid_tax VARCHAR(50),
IN xid_customer VARCHAR(50),
IN xid_user VARCHAR(50))
BEGIN
    INSERT INTO orders(`id_order`, `create_on`, `total_product`, `total_price`, `id_tax`, `id_customer`, `id_user`)
    VALUES(xid_order, xcreate_on, xtotal_product, xtotal_price, xid_tax, xid_customer, xid_user);
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for CreateProduct
-- ----------------------------
DROP PROCEDURE IF EXISTS `CreateProduct`;
delimiter ;;
CREATE PROCEDURE `CreateProduct`(IN xid varchar(50),
IN xproduct_name VARCHAR(100),
IN xdescriptions TEXT,
IN xprice DECIMAL(16, 0),
IN ximg_1 VARCHAR(100),
IN ximg_2 VARCHAR(100),
IN xid_supplier VARCHAR(50),
IN xid_category VARCHAR(50))
BEGIN
    INSERT INTO products(`id_product`, `product_name`, `descriptions`, `price`, `img_1`, `img_2`, `id_supplier`, `id_category`)
    VALUES(xid, xproduct_name, xdescriptions, xprice, ximg_1,ximg_2, xid_supplier, xid_category);
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for CreateSupplier
-- ----------------------------
DROP PROCEDURE IF EXISTS `CreateSupplier`;
delimiter ;;
CREATE PROCEDURE `CreateSupplier`(IN xid_supplier VARCHAR(50),
    IN xsupplier_name VARCHAR(100),
    IN xphone VARCHAR(20),
    IN xaddress VARCHAR(150))
BEGIN
    INSERT INTO suppliers(`id_supplier`, `supplier_name`, `phone`, `address`)
    VALUES(xid_supplier, xsupplier_name, xphone, xaddress);
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for CreateUser
-- ----------------------------
DROP PROCEDURE IF EXISTS `CreateUser`;
delimiter ;;
CREATE PROCEDURE `CreateUser`(IN xid_user VARCHAR(50),
IN xdisplay_name VARCHAR(100),
IN xuser_name VARCHAR(50),
IN xemail VARCHAR(100),
IN xpassword VARCHAR(100),
IN xgender VARCHAR(7),
IN xaddress VARCHAR(150),
IN xis_actived TINYINT(1),
IN xverified_code VARCHAR(30),
IN xid_store VARCHAR(50),
IN xid_manager VARCHAR(50))
BEGIN
    INSERT INTO users(`id_user`, `display_name`, `user_name`, `email`, `password`, `gender`, `address`, `is_actived`, `verified_code`, `id_store`, `id_manager`)
    VALUES(xid_user, xdisplay_name, xuser_name, xemail, xpassword, xgender, xaddress, xis_actived, xverified_code, xid_store, xid_manager);
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for getAllProducts
-- ----------------------------
DROP PROCEDURE IF EXISTS `getAllProducts`;
delimiter ;;
CREATE PROCEDURE `getAllProducts`()
BEGIN
    SELECT * FROM products;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for getAllUser
-- ----------------------------
DROP PROCEDURE IF EXISTS `getAllUser`;
delimiter ;;
CREATE PROCEDURE `getAllUser`(IN xid_manager  VARCHAR(50))
BEGIN
    SELECT * FROM users
    WHERE users.id_manager = xid_manager;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for getProductfollowId
-- ----------------------------
DROP PROCEDURE IF EXISTS `getProductfollowId`;
delimiter ;;
CREATE PROCEDURE `getProductfollowId`(IN xid varchar(50))
BEGIN
    SELECT * FROM products
    WHERE products.id_product=xid;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SearchOrderFollowOrderDate
-- ----------------------------
DROP PROCEDURE IF EXISTS `SearchOrderFollowOrderDate`;
delimiter ;;
CREATE PROCEDURE `SearchOrderFollowOrderDate`(IN xorderdate DATETIME)
BEGIN
    SELECT * FROM orders
    WHERE orders.create_on = xorderdate;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for SearchProductbyNameorDescription
-- ----------------------------
DROP PROCEDURE IF EXISTS `SearchProductbyNameorDescription`;
delimiter ;;
CREATE PROCEDURE `SearchProductbyNameorDescription`(IN xproduct_name VARCHAR(100),
IN xdescription TEXT)
BEGIN
    SELECT * FROM products
    WHERE products.product_name = xproduct_name OR products.descriptions = xdescription;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for UpdateCategoryFollowId
-- ----------------------------
DROP PROCEDURE IF EXISTS `UpdateCategoryFollowId`;
delimiter ;;
CREATE PROCEDURE `UpdateCategoryFollowId`(IN xid_category VARCHAR(50),
IN xcategory_name VARCHAR(100),
IN xdescription VARCHAR(200))
BEGIN
    UPDATE categories
    SET categories.category_name=xcategory_name,
    categories.description=xdescription
    WHERE categories.id_category=xid_category;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for UpdateProduct
-- ----------------------------
DROP PROCEDURE IF EXISTS `UpdateProduct`;
delimiter ;;
CREATE PROCEDURE `UpdateProduct`(IN xid varchar(50),
IN xproduct_name VARCHAR(100),
IN xdescriptions TEXT,
IN xprice DECIMAL(16, 0),
IN ximg_1 VARCHAR(100),
IN ximg_2 VARCHAR(100),
IN xid_supplier VARCHAR(50),
IN xid_category VARCHAR(50))
BEGIN
    UPDATE products
    SET products.product_name = xproduct_name,
        products.descriptions = xdescriptions,
        products.price = xprice,
        products.img_1 = ximg_1,
        products.img_2 = ximg_2,
        products.id_supplier = xid_supplier,
        products.id_category = xid_category
    WHERE products.id_product = xid;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for UpdateProfileUserFollowId
-- ----------------------------
DROP PROCEDURE IF EXISTS `UpdateProfileUserFollowId`;
delimiter ;;
CREATE PROCEDURE `UpdateProfileUserFollowId`(IN xid_user VARCHAR(50),
    IN xdisplay_name VARCHAR(100),
    IN xuser_name VARCHAR(50) ,
    IN xemail VARCHAR(100) ,
    IN xpassword VARCHAR(100),
    IN xgender VARCHAR(7),
    IN xaddress VARCHAR(150) ,
    IN xis_actived TINYINT(1),
    IN xverified_code VARCHAR(30) ,
    IN xid_store VARCHAR(50),
    IN xid_manager VARCHAR(50))
BEGIN
    UPDATE users
    SET users.display_name=xdisplay_name,
        users.user_name=xuser_name,
        users.email=xemail,
        users.password=xpassword,
        users.gender=xgender,
        users.address=xaddress,
        users.is_actived=xis_actived,
        users.verified_code=xverified_code,
        users.id_store=xid_store,
        users.id_manager=xid_manager
    WHERE users.id_user = xid_user;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for UpdateSupplierFollowId
-- ----------------------------
DROP PROCEDURE IF EXISTS `UpdateSupplierFollowId`;
delimiter ;;
CREATE PROCEDURE `UpdateSupplierFollowId`(IN xid_supplier VARCHAR(50),
IN xsupplier_name VARCHAR(100),
IN xphone VARCHAR(20),
IN xaddress VARCHAR(150))
BEGIN
    UPDATE suppliers
    SET supplier_name=xsupplier_name,
    suppliers.phone=xphone,
    suppliers.address=xaddress
    WHERE suppliers.id_supplier = xid_supplier;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
