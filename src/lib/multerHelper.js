const multer = require('multer');
const path = require('path');
const fs = require('fs');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const now = new Date();
    const folderName = `${now.getDate()}-${now.getMonth()}-${now.getFullYear()}`;
    if (!fs.existsSync(`src/app/views/assets/upload/${folderName}`))
      fs.mkdirSync(`src/app/views/assets/upload/${folderName}`);

    cb(null, `src/app/views/assets/upload/${folderName}`);
  },
  filename: (req, file, cb) => {
    cb(
      null,
      `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`
    );
  },
});

const updload = multer({ storage });

module.exports = updload;
