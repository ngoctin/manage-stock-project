const sliceString = value => {
  const arrWords = value.split('-');
  return arrWords[arrWords.length - 1];
};

const renderActionFollowRole = (role, id_product) => {
  if (role === 'MANAGER') {
    return `<a href="" class="mr-2 no-hover">
              <i class="fas fa-edit"></i>
            </a>`;
  }
  if (role === 'ADMIN') {
    return `<a href="/delete-product/${id_product}">
              <i class="fas fa-trash text-danger"></i>
            </a>`;
  }
};

module.exports = {
  sliceString,
  renderActionFollowRole,
};
