const listItemOfStaff = [
  {
    name: 'Tạo đơn hàng',
    classIcon: 'fas fa-tags',
    router: '/create-order',
    active: '',
    role: 'STAFF',
  },
];

const listItemOfManager = [
  {
    name: 'Quản lí đơn hàng',
    classIcon: 'fas fa-cash-register',
    router: '/manage-orders',
    active: '',
    role: 'MANAGER',
  },
  {
    name: 'Quản lí sản phẩm',
    classIcon: 'fas fa-cubes',
    router: '/manage-products',
    active: '',
    role: 'MANAGER',
  },
  {
    name: 'Quản lí nhân viên',
    classIcon: 'fas fa-user-alt',
    router: '/manage-employees',
    active: '',
    role: 'MANAGER',
  },
  {
    name: 'Quản lí nhà khách hàng',
    classIcon: 'fas fa-user-alt',
    router: '/manage-customers',
    active: '',
    role: 'MANAGER',
  },
  {
    name: 'Lập phiếu yêu cầu',
    classIcon: 'fas fa-file-invoice',
    router: '/create-request-bill',
    active: '',
    role: 'MANAGER',
  },
  {
    name: 'Tạo tài khoản',
    classIcon: 'fas fa-plus',
    router: '/insert-account',
    active: '',
    role: 'MANAGER',
  },
  {
    name: 'Thống kê',
    classIcon: 'far fa-chart-bar',
    router: '/manage-statisticals',
    active: '',
    role: 'MANAGER',
  },
];

const listItemOfAdmin = [
  {
    name: 'Quản lí đơn hàng',
    classIcon: 'fas fa-cash-register',
    router: '/manage-orders',
    active: '',
    role: 'ADMIN',
  },
  {
    name: 'Quản lí sản phẩm',
    classIcon: 'fas fa-cubes',
    router: '/manage-products',
    active: '',
    role: 'ADMIN',
  },
  {
    name: 'Quản lí danh mục',
    classIcon: 'fas fa-th-list',
    router: '/manage-categories',
    active: '',
    role: 'ADMIN',
  },
  {
    name: 'Quản lí nhân viên',
    classIcon: 'fas fa-user-alt',
    router: '/manage-employee',
    active: '',
    role: 'ADMIN',
  },
  {
    name: 'Quản lí nhà khách hàng',
    classIcon: 'fas fa-user-alt',
    router: '/manage-customers',
    active: '',
    role: 'ADMIN',
  },
  {
    name: 'Quản lí nhà cung cấp',
    classIcon: 'fas fa-user-alt',
    router: '/manage-suppliers',
    active: '',
    role: 'ADMIN',
  },
  {
    name: 'Tạo tài khoản',
    classIcon: 'fas fa-plus',
    router: '/insert-accounts',
    active: '',
    role: 'ADMIN',
  },
  {
    name: 'Thống kê',
    classIcon: 'far fa-chart-bar',
    router: '/manage-statisticals',
    active: '',
    role: 'ADMIN',
  },
];

module.exports = {
  listItemOfStaff,
  listItemOfManager,
  listItemOfAdmin,
};
