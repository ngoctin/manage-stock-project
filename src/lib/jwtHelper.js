require('dotenv').config();
const jwt = require('jsonwebtoken');

const sign = (payload, privateKey) =>
  new Promise((resolve, reject) => {
    jwt.sign(
      { ...payload },
      privateKey,
      {
        algorithm: 'HS256',
        expiresIn: '1d',
        iss: 'nhom20',
      },
      // eslint-disable-next-line consistent-return
      (error, token) => {
        if (error) return reject(error);

        return resolve(token);
      }
    );
  });

const verify = (token, publicKey) =>
  new Promise((resolve, reject) => {
    jwt.verify(
      token,
      publicKey,
      { algorithms: ['HS256'] },
      (error, decoded) => {
        if (error) return reject(error);

        return resolve(decoded);
      }
    );
  });

const decode = token => jwt.decode(token);

module.exports = {
  sign,
  verify,
  decode,
};
