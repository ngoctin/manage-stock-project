$(document).ready(function() {
  var query_string = null;

  // Call boot box to ask user
  renderDeleteProductBox();

  // Search
  // searchData();
  var app = $.sammy('#app', function() {
    this.around(function(callback) {
      $('#auction-online-product-table').loading('toggle');
      var context = this;
      this.load(`http://${host}/api/v1/products/search`)
        .then(function(items) {
          context.items = items;
        })
        .then(callback);
    });

    this.get('#/tim-kiem/:key', function(context) {
      this.item = this.items[this.params['key']];
      query_string = this.params['key'];
      $.ajax({
        url: `http://${host}/api/v1/products/search?key=` + this.params['key'],
        type: 'GET',
        dataType: 'JSON',
        success: function({ result }) {
          context.app.swap('');
          const html = result.products.map(function(item) {
            return renderRecordOfProductTable(item);
          });

          $('#app')
            .append(html.join(''))
            .ready(function() {
              $('#auction-online-product-table').loading('stop');
            });

          renderDeleteProductBox();

          $('#auction-online-removed-pagination')
            .children('nav')
            .remove();

          if (result.count) {
            $('#auction-online-removed-pagination')
              .append(
                renderPaginationItem(
                  result.current_page,
                  result.current_page + 1,
                  result.current_page + 2,
                  perPage,
                  result.count
                )
              )
              .ready(function() {
                addClickDOMForPaginationItem(
                  query_string,
                  result.count,
                  perPage
                );
              });
          }
        },
      });
    });

    this.notFound = function() {
      addClickDOMForPaginationItem(query_string, result.count, perPage);
    };
  });

  $('#auction-online-search-button').click(function(e) {
    var query_string = $('#auction-online-search-input').val();
    $(this)
      .children('a')
      .attr('href', `#/tim-kiem/${query_string}`);

    app.run('#/tim-kiem/:key');
  });
});

function addClickDOMForPaginationItem(query_string, count, perPage) {
  $('.page-item')
    .off()
    .click(function(e) {
      e.preventDefault();
      var currentClickEvent = $(this);
      var page = $(this).attr('data-page');
      $('#auction-online-product-table').loading('toggle');
      axios({
        url: `/api/v1/products/search?key=${query_string}&page=${page}`,
        method: 'GET',
      }).then(function({ data }) {
        $('#app')
          .children()
          .remove();

        var newPageHtml = data.result.products.map(item =>
          renderRecordOfProductTable(item)
        );

        $('#app')
          .append(newPageHtml.join(''))
          .ready(function() {
            renderDeleteProductBox();
          })
          .ready(function() {
            if (
              page * perPage < count &&
              (parseInt(page) + 1) * perPage < count
            ) {
              if (page == 1) {
                $('#auction-online-click-previous')
                  .removeClass('auction-online-display-block')
                  .addClass('auction-online-display-none');
              }

              if (page > 1) {
                $('#auction-online-click-previous')
                  .removeClass('auction-online-display-none')
                  .addClass('auction-online-display-block');
              }

              $('#auction-online-click-previous').attr(
                'data-page',
                parseInt(page) - 1
              );

              $('#auction-online-click-one').attr('data-page', parseInt(page));

              $('#auction-online-click-two').attr(
                'data-page',
                parseInt(page) + 1
              );

              $('#auction-online-click-third').attr(
                'data-page',
                parseInt(page) + 2
              );

              if ((parseInt(page) + 1) * perPage < count) {
                $('#auction-online-click-one')
                  .children()
                  .text(parseInt(page));

                $('#auction-online-click-two')
                  .children()
                  .text(parseInt(page) + 1);

                $('#auction-online-click-third')
                  .children()
                  .text(parseInt(page) + 2);
              }

              $('#auction-online-click-next')
                .removeClass('auction-online-display-none')
                .addClass('auction-online-display-block');
              $('#auction-online-click-next').attr(
                'data-page',
                parseInt(page) + 2
              );
            } else {
              $('#auction-online-click-next')
                .removeClass('auction-online-display-block')
                .addClass('auction-online-display-none');
            }
          })
          .ready(function() {
            $('#auction-online-product-table').loading('stop');
          });
      });
    });
}

function renderRecordOfProductTable(item) {
  return `<tr class="auction-online-transition">
            <th data-id="${item.id_product}" scope="row">${'***' +
    item.id_product.slice(-4)}</th>
            <td class="position-relative d-flex align-items-center">
              ${
                !item.is_showed
                  ? `<div class="position-absolute auction-online-removed-icon"></div>`
                  : new Date().getTime() < new Date(item.ended_at).getTime()
                  ? `<div class="position-absolute auction-online-online-icon"></div>`
                  : `<div class="position-absolute auction-online-offline-icon"></div>`
              }
              ${item.product_name}
            </td>
            <td>${item.starting_price}</td>
            <td>${item.step_price}</td>
            <td>123</td>
            <td class="text-right">
              ${
                item.is_showed
                  ? `<a class="d-inline-block ml-2 auction-online-delete-product" href=""><i data-toggle="tooltip" data-placement="bottom" title="Xóa sản phẩm"class="far fa-trash-alt text-danger"></i></a>`
                  : ''
              }
            </td>
          </tr>`;
}

function searchData() {
  $('#auction-online-search').submit(function(e) {
    e.preventDefault();
    var app = $.sammy('#app', function() {
      this.get('/auction-online/admin/tat-ca-san-pham/tim-kiem?key=123');
    });
    return false;
  });
}

function renderPaginationItem(pageOne, pageTwo, pageThird, perPage, count) {
  console.log(
    pageOne * perPage < count
      ? `auction-online-display-block`
      : `auction-online-display-none`
  );
  return `<nav aria-label="Page navigation example">
            <ul class="pagination justify-content-end">
              <li id="auction-online-click-previous" class="page-item ${
                !(pageOne - 1)
                  ? `auction-online-display-none`
                  : `auction-online-display-block`
              }" data-page=${pageOne - 1}>
                <a class="page-link" href="#" aria-label="Previous">
                  <span aria-hidden="true">&laquo;</span>
                </a>
              </li>
              <li class="page-item active" id="auction-online-click-one" data-page="${pageOne}"><a class="page-link" href="#">${pageOne}</a></li>
              <li class="page-item ${
                pageOne * perPage < count
                  ? `auction-online-display-block`
                  : `auction-online-display-none`
              }" id="auction-online-click-two" data-page="${pageTwo}"><a class="page-link" href="#">${pageTwo}</a></li>
              <li class="page-item ${
                pageOne * perPage < count
                  ? `auction-online-display-block`
                  : `auction-online-display-none`
              }" id="auction-online-click-third" data-page="${pageThird}"><a class="page-link" href="#">${pageThird}</a></li>
              <li class="page-item ${
                pageOne * perPage < count
                  ? `auction-online-display-block`
                  : `auction-online-display-none`
              }" id="auction-online-click-next" data-page=${pageThird + 1}>
                <a class="page-link" href="#" aria-label="Next">
                  <span aria-hidden="true">&raquo;</span>
                </a>
              </li>
            </ul>
          </nav>`;
}

function renderDeleteProductBox() {
  $('.auction-online-delete-product').click(function(e) {
    e.preventDefault();
    var currentElement = $(this);
    bootbox.confirm({
      message: 'Bạn có muốn gỡ bỏ sản phẩm này',
      buttons: {
        confirm: {
          label: 'Gỡ bỏ',
          className: 'btn-danger auction-online-customize-btn',
        },
        cancel: {
          label: 'Hủy',
          className: 'btn-secondary auction-online-customize-btn',
        },
      },
      callback: function(result) {
        var idProduct = currentElement
          .parents('tr.auction-online-transition')
          .children('th')
          .attr('data-id');
        if (result) {
          $('#auction-online-product-table').loading('toggle');
          axios({
            url: `/api/v1/products/${idProduct}/hide`,
            method: 'PUT',
          }).then(function({ data }) {
            if (
              window.location.pathname ===
              '/auction-online/admin/tat-ca-san-pham'
            ) {
              setTimeout(function() {
                $('#auction-online-product-table').loading('stop');

                // Remove icon online/offline
                currentElement
                  .parents('tr.auction-online-transition')
                  .children(':eq(1)')
                  .children()
                  .remove();

                currentElement
                  .parents('tr.auction-online-transition')
                  .children(':eq(1)')
                  .append(
                    `<div class="position-absolute auction-online-removed-icon"></div>`
                  );

                // Remove delete button
                currentElement
                  .parents('tr.auction-online-transition')
                  .children(':eq(5)')
                  .children()
                  .remove();
              }, 2000);
            }
          });
        }
      },
    });
  });
}
