$(document).ready(function() {
  var orderCategories = null;

  $('.dd').nestable({
    maxDepth: 2,
  });
  $('#auction-online-change-order').on('shown.bs.modal', function() {
    var hasChildren = $('.dd .dd-list').children().length;
    if (parseInt(hasChildren) == 0) {
      $('#auction-online-change-categories').show();
      loadCategories();
    } else if (change) {
      $('#auction-online-change-categories').show();
      loadCategories();
    }
  });

  $('.dd').on('change', function() {
    orderCategories = $('.dd').nestable('serialize');
    localStorage.setItem('data', JSON.stringify(orderCategories));
  });

  $('#auction-online-save-change').on('click', function(e) {
    $('#auction-online-change-categories').show();
    $('.dd .dd-list')
      .children()
      .remove();
    axios({
      url: '/api/v1/categories/handle',
      method: 'POST',
      data: {
        data: localStorage.getItem('data'),
      },
    })
      .then(function({ data }) {
        loadCategories();
        $.toast({
          heading: 'Thành công',
          icon: 'success',
          loader: true, // Change it to false to disable loader
          loaderBg: '#9EC600', // To change the background
        });
      })
      .catch(function(error) {
        console.log(error);
      });
  });
});

function loadCategories() {
  axios({
    url: '/api/v1/categories',
    method: 'GET',
  })
    .then(function({ data }) {
      var arrParent = data.result.categories.map(function(item) {
        return `<li id="${
          item.id_category
        }" class="dd-item" data-id="${item.id_category}">
                              <div class="dd-handle">${item.category_name}</div>
                              ${
                                item.children.length
                                  ? `<ol class="dd-list"></ol>`
                                  : ''
                              }
                          </li>`;
      });

      $('.dd-list')
        .html(arrParent.join(''))
        .ready(function() {
          var arrChilren = data.result.categories.filter(
            item => item.children.length
          );

          arrChilren.forEach(element => {
            element.children.forEach(cate => {
              $(`#${element.id_category} ol.dd-list`)
                .append(`<li class="dd-item" data-id="${cate.id_category}">
                          <div class="dd-handle">${cate.category_name}</div>
                      </li>`);
            });
          });
        })
        .ready(function() {
          $('#auction-online-change-categories').hide();
        });
    })
    .catch(function(error) {
      $.toast({
        heading: `${'Sorry! Server đang bận.'}`,
        icon: 'error',
        loader: true, // Change it to false to disable loader
        loaderBg: '#9EC600', // To change the background
        bgColor: '#ff3b27',
      });
    });
}
