$(document).ready(function() {
  $('.auction-online-inferior').change(function(e) {
    var currentSelecteElement = $(this);
    var emailSeller = $(this)
      .parents('tr')
      .children('td:eq(0)')
      .text();
    bootbox.confirm({
      message: `Hạ cấp người dùng <b>${emailSeller}</b> ?`,
      buttons: {
        confirm: {
          label: 'Xác nhận',
          className: 'btn-danger auction-online-customize-btn',
        },
        cancel: {
          label: 'Hủy',
          className: 'btn-secondary auction-online-customize-btn',
        },
      },
      callback: function(result) {
        if (!result)
          currentSelecteElement.children('option:first').prop('selected', true);
        else {
          var idSeller = currentSelecteElement.children('option:eq(1)').val();
          var idUserSeller = currentSelecteElement
            .parents('tr')
            .children('th:eq(0)')
            .attr('data-id');

          axios({
            url: '/api/v1/roles',
            method: 'DELETE',
            data: {
              idRole: idSeller,
              idUser: idUserSeller,
            },
          })
            .then(function({ data }) {
              currentSelecteElement.prop('disabled', true);
              $.toast({
                heading: 'Thành công',
                icon: 'success',
                loader: true, // Change it to false to disable loader
                loaderBg: '#9EC600', // To change the background
              });
            })
            .catch(function(err) {
              $.toast({
                heading: 'Bạn đã thực hiện hành động này',
                icon: 'error',
                loader: true, // Change it to false to disable loader
                loaderBg: '#9EC600', // To change the background
                bgColor: '#ff3b27',
              });
            });
        }
      },
    });
  });
});
