$(".items").sortable({
  connectWith: ".items",
  placeholder: "placeholder",
  update: function (event, ui) {
    // update
    console.log('update');
  },
  start: function (event, ui) {
    if (ui.helper.hasClass('second-level')) {
      ui.placeholder.removeClass('placeholder');
      ui.placeholder.addClass('placeholder-sub');
    }
    else {
      ui.placeholder.removeClass('placeholder-sub');
      ui.placeholder.addClass('placeholder');
    }
    console.log('Start');
  },
  sort: function (event, ui) {
    var pos;
    if (ui.helper.hasClass('second-level')) {
      pos = ui.position.left + 20;
      $('#cursor').text(ui.position.left + 20);
    }
    else {
      pos = ui.position.left;
      $('#cursor').text(ui.position.left);
    }
    if (pos >= 32 && !ui.helper.hasClass('second-level')) {
      ui.placeholder.removeClass('placeholder');
      ui.placeholder.addClass('placeholder-sub');
      ui.helper.addClass('second-level');
    }
    else if (pos < 25 && ui.helper.hasClass('second-level')) {
      ui.placeholder.removeClass('placeholder-sub');
      ui.placeholder.addClass('placeholder');
      ui.helper.removeClass('second-level');
    }
    console.log('Sort');
  },
  stop: function (event, ui) {
    console.log('stop');
  }
});
$(".item").droppable({
  accept: ".item",
  hoverClass: "dragHover",
  drop: function (event, ui) {
    console.log('drop');
  },
  over: function (event, ui) {
    // over
  },
  activate: function (event, ui) {
    // activate
  }
});
