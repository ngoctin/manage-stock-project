$(document).ready(function() {
  var email = null;
  var idMessage = null;
  var idUser = null;
  var currentElement = null;
  var record = null;
  var infoUser = null;

  $('.auction-online-upgrade-click').click(function(e) {
    e.preventDefault();

    record = $(this);

    email = $(this)
      .parents('tr')
      .children('td:eq(2)')
      .text();
    idMessage = $(this)
      .parents('tr')
      .attr('data-id');
    idUser = $(this)
      .parents('tr')
      .children('td:eq(0)')
      .attr('data-user');
    infoUser = $(this)
      .parents('tr')
      .children('td:eq(5)')
      .children('input')
      .val();
  });

  $('#auction-online-upgrade-btn').click(function(e) {
    e.preventDefault();

    currentElement = $(this);

    $(this).addClass('d-none');

    $('#auction-online-upgrade-btn-lock').removeClass('d-none');
    $('#auction-online-upgrade-btn-lock').addClass('d-inline-block');

    $('#auction-online-button-cancel-hide').hide();

    axios({
      url: '/api/v1/roles',
      method: 'POST',
      data: {
        data: 'SUCCESS !',
        email,
        idMessage,
        idUser,
      },
    })
      .then(function({ data }) {
        $('#auction-online-upgrade').modal('hide');

        var countRequest = parseInt(record.parents('tbody').children().length);

        record.parents('tr').remove();
        $('#auction-badge').text(countRequest - 1);

        $.toast({
          heading: 'Nâng cấp thành công',
          icon: 'success',
          loader: true, // Change it to false to disable loader
          loaderBg: '#9EC600', // To change the background
        });
      })
      .catch(function(err) {
        $.toast({
          heading: 'Server đang bận.',
          icon: 'error',
          loader: true, // Change it to false to disable loader
          loaderBg: '#9EC600', // To change the background
          bgColor: '#ff3b27',
        });
      });
  });

  $('#auction-online-upgrade').on('show.bs.modal', function() {
    var infoOfUser = [];
    var data = JSON.parse(infoUser);
    infoOfUser.push(`<li>Id: ${data.id_user}</li>`);
    infoOfUser.push(`<li>Tên: ${data.first_name} ${data.last_name}</li>`);
    infoOfUser.push(`<li>Email: ${data.email}</li>`);
    infoOfUser.push(`<li>Địa chỉ: ${data.address}</li>`);

    //console.log(data);

    $('#auction-online-upgrade .modal-dialog .modal-content')
      .children('div.modal-body')
      .html(infoOfUser.join(''));
  });

  $('#auction-online-upgrade').on('hidden.bs.modal', function() {
    if (currentElement) {
      currentElement.removeClass('d-none');
    }
    $('#auction-online-button-cancel-hide').show();
    $('#auction-online-upgrade-btn-lock').removeClass('d-inline-block');
    $('#auction-online-upgrade-btn-lock').addClass('d-none');
  });
});
