$(document).ready(function() {
  $('#auction-online-login').validate({
    rules: {
      email: 'required',
      password: 'required',
    },
    messages: {
      email: 'Vui lòng nhập email',
      password: 'Vui lòng nhập mật khẩu',
    },
    errorPlacement: function(error, element) {
      if ($(element).next()) {
        $(element)
          .next()
          .remove();
      }
      $(element).parent().append(`<div class="auction-online-change-color">
          <i class="fas fa-info-circle"></i>
          ${$(error[0]).html()}
        </div>`);
    },
    errorClass: 'auction-online-error',
    errorElement: 'div',
    validClass: 'testting-123',
  });
});
