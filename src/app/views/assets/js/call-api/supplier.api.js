$(document).ready(function() {
  var idSupplier = null;
  var nameSupplier = null;
  var addressSupplier = null;
  var phoneSupplier = null;
  var currentElement = null;

  $('.manage-stock-supplier-edit').click(function(e) {
    currentElement = $(this);

    idSupplier = $(this)
      .parents('tr')
      .children('td:eq(0)')
      .attr('data-id');

    nameSupplier = $(this)
      .parents('tr')
      .children('td:eq(1)')
      .text();

    phoneSupplier = $(this)
      .parents('tr')
      .children('td:eq(2)')
      .text();

    addressSupplier = $(this)
      .parents('tr')
      .children('td:eq(3)')
      .text();
  });

  $('#manage-stock-supplier-update').on('show.bs.modal', function(e) {
    $('#manage-stock-input-id-sup').val(idSupplier.trim());
    $('#manage-stock-input-name-sup').val(nameSupplier.trim());
    $('#manage-stock-input-address-sup').val(addressSupplier.trim());
    $('#manage-stock-input-phone-sup').val(phoneSupplier.trim());
  });

  $('#manage-stock-update-supplier-form').validate({
    rules: {
      idSupplier: 'required',
      nameSupplier: 'required',
      addressSupplier: 'required',
      phoneSupplier: 'required',
    },
    messages: {
      idSupplier: 'Không được để trống',
      nameSupplier: 'Vui lòng nhập tên nhà cung cấp',
      addressSupplier: 'Vui lòng nhập địa chỉ',
      phoneSupplier: 'Vui lòng nhập số điện thoại',
    },
    errorClass: 'manage-stock-error',
    errorElement: 'div',
    errorPlacement: function(error, element) {
      if ($(element).next()) {
        $(element)
          .next()
          .remove();
      }

      $(element)
        .after(`<div class="d-flex align-items-center auction-online-customize-error">
                    <i class="fas fa-info-circle mr-2"></i>
                    ${error[0].innerHTML}
                </div>`);
    },
    success: function(label, element) {
      $(element)
        .next()
        .remove();
    },
    submitHandler: function() {
      idSupplier = $('#manage-stock-input-id-sup').val();
      nameSupplier = $('#manage-stock-input-name-sup').val();
      addressSupplier = $('#manage-stock-input-address-sup').val();
      phoneSupplier = $('#manage-stock-input-phone-sup').val();

      var data = {
        idSupplier: $('#manage-stock-input-id-sup').val(),
        nameSupplier: $('#manage-stock-input-name-sup').val(),
        addressSupplier: $('#manage-stock-input-address-sup').val(),
        phoneSupplier: $('#manage-stock-input-phone-sup').val(),
      };

      $('#manage-stock-update-sup').addClass('d-none');
      $('#manage-stock-loading-sup').removeClass('d-none');

      fetch('/suppliers', {
        method: 'PUT',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then(function(response) {
          return response.json();
        })
        .then(function(data) {
          if (data.status === 200 && data.code === 'USR_03') {
            setTimeout(function() {
              $.toast({
                heading: 'Cập nhập thành công',
                icon: 'success',
                loader: true, // Change it to false to disable loader
                loaderBg: '#9EC600', // To change the background
              });

              $('#manage-stock-update-sup').removeClass('d-none');
              $('#manage-stock-loading-sup').addClass('d-none');
            }, 2000);
          } else {
            $.toast({
              heading: 'BAD REQUEST',
              icon: 'error',
              loader: true, // Change it to false to disable loader
              loaderBg: '#9EC600', // To change the background
              bgColor: '#ff3b27',
            });
          }
        })
        .catch(function(error) {
          $.toast({
            heading: 'ERROR NETWORK',
            icon: 'error',
            loader: true, // Change it to false to disable loader
            loaderBg: '#9EC600', // To change the background
            bgColor: '#ff3b27',
          });
        });
    },
  });

  $('#manage-stock-supplier-update').on('hidden.bs.modal', function(e) {
    currentElement.parents('tr').addClass('manage-stock-update');

    currentElement
      .parents('tr')
      .children('td:eq(1)')
      .text(nameSupplier);

    currentElement
      .parents('tr')
      .children('td:eq(2)')
      .text(phoneSupplier);

    currentElement
      .parents('tr')
      .children('td:eq(3)')
      .text(addressSupplier);

    setTimeout(function() {
      currentElement.parents('tr').removeClass('manage-stock-update');
    }, 1000);
  });

  // Delete a supplier
  $('.manage-stock-supplier-delete').click(function(e) {
    currentElement = $(this);

    nameSupplier = $(this)
      .parents('tr')
      .children('td:eq(1)')
      .text();

    idSupplier = $(this)
      .parents('tr')
      .children('td:eq(0)')
      .attr('data-id');
  });

  $('#manage-stock-supplier-delete').on('show.bs.modal', function() {
    $('#manage-stock-show-info').html(
      `Bạn có muốn xóa bỏ nhà cung cấp <b>${nameSupplier}</b> này.`
    );
  });

  $('#manage-stock-delete-btn-sup').click(function(e) {
    var currentElement1 = $(this);
    $(this).addClass('d-none');
    $('#manage-stock-delete-loading-sup').removeClass('d-none');

    var data = {
      idSupplier: idSupplier,
    };

    fetch('/suppliers', {
      method: 'DELETE',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then(function(response) {
        return response.json();
      })
      .then(function(data) {
        if (data.status === 200 || data.code === 'USR_03') {
          setTimeout(function() {
            $.toast({
              heading: 'Cập nhập thành công',
              icon: 'success',
              loader: true, // Change it to false to disable loader
              loaderBg: '#9EC600', // To change the background
            });

            currentElement1.removeClass('d-none');
            $('#manage-stock-delete-loading-sup').addClass('d-none');

            $('#manage-stock-supplier-delete').modal('hide');
          }, 2000);
        } else {
          $.toast({
            heading: 'BAD REQUEST',
            icon: 'error',
            loader: true, // Change it to false to disable loader
            loaderBg: '#9EC600', // To change the background
            bgColor: '#ff3b27',
          });
        }
      })
      .catch(function(err) {
        $.toast({
          heading: 'ERROR NETWORK',
          icon: 'error',
          loader: true, // Change it to false to disable loader
          loaderBg: '#9EC600', // To change the background
          bgColor: '#ff3b27',
        });
      });
  });

  $('#manage-stock-supplier-delete').on('hidden.bs.modal', function() {
    currentElement.parents('tr').remove();
  });
});
