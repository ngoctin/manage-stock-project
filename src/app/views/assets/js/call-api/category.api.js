$(document).ready(function() {
  var idCategory = null;
  var nameCategory = null;
  var description = null;
  var currentElement = null;

  $('.manage-stock-edit').click(function() {
    currentElement = $(this);

    idCategory = $(this)
      .parents('tr')
      .children('td:eq(0)')
      .attr('data-id');

    nameCategory = $(this)
      .parents('tr')
      .children('td:eq(1)')
      .text();

    description = $(this)
      .parents('tr')
      .children('td:eq(2)')
      .text();
  });

  // Delete categories
  $('.manage-stock-delete').click(function(e) {
    currentElement = $(this);

    nameCategory = $(this)
      .parents('tr')
      .children('td:eq(1)')
      .text();

    idCategory = $(this)
      .parents('tr')
      .children('td:eq(0)')
      .attr('data-id');
  });

  $('#manage-stock-delete-category').on('show.bs.modal', function(e) {
    $('#manage-stock-modal-body').html(
      `Bạn có muốn xóa bỏ danh mục <b>${nameCategory}</b> này.`
    );
  });

  $('#manage-stock-btn-delete').click(function(e) {
    var clickedElement = $(this);

    var data = {
      idCategory: idCategory,
    };

    clickedElement.addClass('d-none');
    $('#manage-stock-btn-delete-loading').removeClass('d-none');

    fetch('/categories', {
      method: 'DELETE',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then(function(response) {
        return response.json();
      })
      .then(function(data) {
        if (data.status === 200 && data.code === 'USR_03') {
          setTimeout(function() {
            $.toast({
              heading: 'Xóa danh mục thành công',
              icon: 'success',
              loader: true, // Change it to false to disable loader
              loaderBg: '#9EC600', // To change the background
            });

            currentElement.parents('tr').remove();

            $('#manage-stock-delete-category').modal('hide');

            clickedElement.removeClass('d-none');
            $('#manage-stock-btn-delete-loading').addClass('d-none');
          }, 2000);
        } else {
          $.toast({
            heading: 'BAD REQUEST',
            icon: 'error',
            loader: true, // Change it to false to disable loader
            loaderBg: '#9EC600', // To change the background
            bgColor: '#ff3b27',
          });
        }
      })
      .catch(function(error) {
        $.toast({
          heading: 'ERROR NETWORK',
          icon: 'error',
          loader: true, // Change it to false to disable loader
          loaderBg: '#9EC600', // To change the background
          bgColor: '#ff3b27',
        });
      });
  });

  // Update categories
  $('#update-category-modal').on('show.bs.modal', function(e) {
    $('#auction-online-id-category-update').val(idCategory);
    $('#auction-online-name-category-update').val(
      nameCategory.replace(/\s\s+/g, '').trim()
    );
    $('#manage-stock-description').val(description.replace(/\s\s+/g, ''));
  });

  // Update categories
  $('#auction-online-form-update').validate({
    rules: {
      idCategory: 'required',
      nameCategory: 'required',
      descrition: 'required',
    },
    messages: {
      idCategory: 'Không được để trống',
      nameCategory: 'Vui lòng điền tên danh mục',
      descrition: 'Vui lòng mô tả danh mục',
    },
    errorClass: 'manage-stock-error',
    errorElement: 'div',
    errorPlacement: function(error, element) {
      if ($(element).next()) {
        $(element)
          .next()
          .remove();
      }
      element.after(`<div class="d-flex align-items-center auction-online-customize-error">
                        <i class="fas fa-info-circle mr-2"></i>
                        ${error[0].innerHTML}
                     </div>`);
    },
    success: function(lable, element) {
      $(element)
        .next()
        .remove();
    },
    submitHandler: function(form) {
      var data = {
        idCategory: $('#auction-online-id-category-update').val(),
        categoryName: $('#auction-online-name-category-update').val(),
        description: $('#manage-stock-description').val(),
      };

      $('#manage-stock-loading').removeClass('d-none');
      $('#manage-stock-update').addClass('d-none');

      fetch('/categories', {
        method: 'PUT',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then(function(response) {
          return response.json();
        })
        .then(function(data) {
          if (data.status === 200 && data.code === 'USR_03') {
            setTimeout(function() {
              $('#update-category-modal').modal('hide');

              currentElement
                .parents('tr')
                .children('td:eq(1)')
                .text($('#auction-online-name-category-update').val());

              currentElement
                .parents('tr')
                .children('td:eq(2)')
                .text($('#manage-stock-description').val());

              $('#manage-stock-loading').addClass('d-none');
              $('#manage-stock-update').removeClass('d-none');

              currentElement.parents('tr').addClass('manage-stock-update');

              setTimeout(function() {
                currentElement.parents('tr').removeClass('manage-stock-update');
              }, 1000);

              $.toast({
                heading: 'Cập nhập thành công',
                icon: 'success',
                loader: true, // Change it to false to disable loader
                loaderBg: '#9EC600', // To change the background
              });
            }, 2000);
          } else {
            $.toast({
              heading: 'Server đang bận.',
              icon: 'error',
              loader: true, // Change it to false to disable loader
              loaderBg: '#9EC600', // To change the background
              bgColor: '#ff3b27',
            });
          }
        })
        .catch(function(err) {
          console.log(err);
          $.toast({
            heading: 'ERROR NETWORK',
            icon: 'error',
            loader: true, // Change it to false to disable loader
            loaderBg: '#9EC600', // To change the background
            bgColor: '#ff3b27',
          });
        });
    },
  });
});
