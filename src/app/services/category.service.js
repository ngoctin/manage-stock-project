const config = require('config');

const Result = require('folktale/result');
const R = require('ramda');

const db = require('../../database/models');

const { Op } = db.Sequelize;

const insertACategory = async ({ idCategory, categoryName, description }) => {
  try {
    const result = await db.sequelize.query(
      'CALL CreateCategory(:id_category, :category_name, :description)',
      {
        replacements: {
          id_category: idCategory,
          category_name: categoryName,
          description,
        },
        type: db.sequelize.QueryTypes.INSERT,
      }
    );

    return Promise.resolve(Result.Ok(result));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

const loadAllCatogories = async page => {
  try {
    const result = await db.categories.findAndCountAll({
      limit: config.get('CONFIGURATION.PER_PAGE'),
      offset: config.get('CONFIGURATION.PER_PAGE') * (page - 1),
    });

    result.rows = result.rows.map(category => {
      return {
        ...R.path(['dataValues'])(category),
      };
    });

    return Promise.resolve(Result.Ok(result));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

const loadAllCatogoriesNoPage = async () => {
  try {
    const categories = await db.categories.findAll();

    const returnResult = categories.map(category => {
      return {
        ...R.path(['dataValues'])(category),
      };
    });

    return Promise.resolve(Result.Ok(returnResult));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

/* eslint-disable-next-line consistent-return */
const searchCategoriesFollowName = async (perPage, query_string, page) => {
  try {
    /* eslint-disable radix */
    const result = await db.categories.findAndCountAll({
      offset: (parseInt(page) - 1) * parseInt(perPage),
      limit: parseInt(perPage),
      where: {
        category_name: {
          [Op.like]: `%${query_string}%`, // eslint-disable-line camelcase
        },
      },
    });

    result.rows = result.rows.map(category => {
      return {
        ...R.path(['dataValues'])(category),
      };
    });

    return Promise.resolve(Result.Ok(result));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

const updateACategory = async (idCategory, categoryName, description) => {
  try {
    const updatedCategory = await db.categories.update(
      {
        category_name: categoryName,
        description,
      },
      {
        where: {
          id_category: idCategory,
        },
      }
    );

    return Promise.resolve(Result.Ok(updatedCategory));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

const deleteACategory = async idCategory => {
  try {
    const result = await db.categories.destroy({
      where: {
        id_category: idCategory,
      },
    });

    return Promise.resolve(Result.Ok(result));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

module.exports = {
  insertACategory,
  loadAllCatogories,
  searchCategoriesFollowName,
  updateACategory,
  deleteACategory,
  loadAllCatogoriesNoPage,
};
