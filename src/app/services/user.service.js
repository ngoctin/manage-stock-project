const Result = require('folktale/result');
const R = require('ramda');

const db = require('../../database/models');

const { Op } = db.Sequelize;

const findUserByEmail = async email => {
  try {
    const user = await db.users.findOne({
      where: {
        email,
      },
    });

    return Promise.resolve(Result.Ok(user));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

const getRoleOfUser = async idUser => {
  try {
    const user = await db.users.findOne({
      where: {
        id_user: idUser,
      },
      include: [
        {
          model: db.roles,
          through: 'user_role',
        },
        {
          model: db.stores,
          on: {
            id_store: {
              [Op.col]: 'users.id_store',
            },
          },
        },
      ],
      as: 'user1',
    });

    const returnResult = {
      ...R.path(['dataValues'])(user),
      roles: R.path(['dataValues', 'roles'])(user).map(role => {
        return R.path(['dataValues', 'role_name'])(role);
      }),
      store: {
        ...R.path(['dataValues', 'store', 'dataValues'])(user),
      },
    };

    return Promise.resolve(Result.Ok(returnResult));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

const findUserWithRoleById = async idUser => {
  try {
    const role = await db.user_role.findOne({
      where: {
        id_user: idUser,
      },
      include: [
        {
          model: db.roles,
        },
      ],
    });

    const returnResult = [
      R.path(['dataValues', 'role', 'dataValues', 'role_name'])(role),
    ];

    return Promise.resolve(Result.Ok(returnResult));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

const insertUserRole = async ({ id_user, id_role }) => {
  try {
    const result = await db.sequelize.query(
      `INSERT INTO user_role(id_role,id_user) VALUES("${id_role}", "${id_user}");`,
      {
        type: db.sequelize.QueryTypes.INSERT,
      }
    );
    return Promise.resolve(Result.Ok(result));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

module.exports = {
  findUserByEmail,
  getRoleOfUser,
  findUserWithRoleById,
  insertUserRole,
};
