const Result = require('folktale/result');
const db = require('../../database/models');

// console.log(db);
/* eslint-disable */
const loadOrders = async () => {
  try {
    const result = await db.sequelize.query(`select o.id_order, c.first_name, c.last_name, percentage_tax, total_price, create_on from (orders o join customers c on o.id_customer = c.id_customer) join tax t on t.id_tax = o.id_tax`, {
      type: db.sequelize.QueryTypes.SELECT
    });
    return Promise.resolve(Result.Ok(result));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

const loadOrderDetail = async (id) => {
  try {
    const result = await db.sequelize.query(
      `select o.id_order, p.product_name, od.quantity_order, od.unit_cost, t.percentage_tax, o.total_price
    from ((orders o join orders_detail od on o.id_order = od.id_order) join products p on p.id_product = od.id_product) join tax t on o.id_tax = t.id_tax
    where o.id_order= "${id}"`, {
      type: db.sequelize.QueryTypes.SELECT
    });
    return Promise.resolve(Result.Ok(result));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
}

const createOrder = async ({
  id_order,
  create_on,
  total_product,
  total_price,
  id_tax,
  id_customer,
  id_user,
}) => {
  try {
    const result = await db.sequelize.query(
      'call CreateOrder(:xid_order,:xcreate_on,:xtotal_product,:xtotal_price,:xid_tax,:xid_customer,:xid_user)',
      {
        replacements: {
          xid_order: id_order,
          xcreate_on: create_on,
          xtotal_product: total_product,
          xtotal_price: total_price,
          xid_tax: id_tax,
          xid_customer: id_customer,
          xid_user: id_user,
        },
        type: db.sequelize.QueryTypes.INSERT
      }
    );
    return Promise.resolve(Result.Ok(result));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
}

const createOrderDetail = async ({
  id_order,
  id_order_detail,
  created_date,
  quantity_order,
  unit_cost,
  line_total,
  id_product,
}) => {
  try {
    const result = await db.sequelize.query(
      'call CreateDetailOrder(:xid_order,:xid_order_detail,:xcreated_date,:xquantity_order,:xunit_cost,:xline_total,:xid_product)',
      {
        replacements: {
          xid_order: id_order,
          xid_order_detail: id_order_detail,
          xcreated_date: created_date,
          xquantity_order: quantity_order,
          xunit_cost: unit_cost,
          xline_total: line_total,
          xid_product: id_product,
        },
        type: db.sequelize.QueryTypes.INSERT
      }
    );
    return Promise.resolve(Result.Ok(result));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
}

module.exports = {
  loadOrders,
  loadOrderDetail,
  createOrder,
  createOrderDetail,
};
