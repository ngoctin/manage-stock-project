const config = require('config');
const Result = require('folktale/result');
const R = require('ramda');

const db = require('../../database/models');

const { Op } = db.Sequelize;

const getAllSuppliers = async page => {
  try {
    const suppliers = await db.suppliers.findAndCountAll({
      offset: (page - 1) * config.get('CONFIGURATION.PER_PAGE'),
      limit: config.get('CONFIGURATION.PER_PAGE'),
    });

    suppliers.rows = suppliers.rows.map(supplier => {
      return {
        ...R.path(['dataValues'])(supplier),
      };
    });

    return Promise.resolve(Result.Ok(suppliers));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

const insertSupplier = async data => {
  try {
    const supplier = await db.suppliers.create({
      ...data,
    });

    return Promise.resolve(Result.Ok(supplier));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

const updateCategoryById = async (idSupplier, data) => {
  try {
    const result = await db.suppliers.update(
      {
        ...data,
      },
      {
        where: {
          id_supplier: idSupplier,
        },
      }
    );

    return Promise.resolve(Result.Ok(result));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

const deleteCategoryById = async idSupplier => {
  try {
    const result = await db.suppliers.destroy({
      where: { id_supplier: idSupplier },
    });

    return Promise.resolve(Result.Ok(result));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

/* eslint-disable-next-line camelcase */
const searchSuppliers = async (query_string, perPage, page) => {
  try {
    const searchedResult = await db.suppliers.findAndCountAll({
      where: {
        [Op.or]: [
          {
            supplier_name: {
              [Op.like]: `%${query_string}%`, // eslint-disable-line
            },
          },
          {
            phone: {
              [Op.like]: `%${query_string}%`, // eslint-disable-line
            },
          },
        ],
      },
      offset: (page - 1) * parseInt(perPage), // eslint-disable-line
      limit: parseInt(perPage), // eslint-disable-line
    });

    searchedResult.rows = searchedResult.rows.map(item => {
      return {
        ...R.path(['dataValues'])(item),
      };
    });

    return Promise.resolve(Result.Ok(searchedResult));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

const getAllSuppliersNoPage = async () => {
  try {
    const suppliers = await db.suppliers.findAll();

    const returnResult = suppliers.map(supplier => {
      return {
        ...R.path(['dataValues'])(supplier),
      };
    });

    return Promise.resolve(Result.Ok(returnResult));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

module.exports = {
  getAllSuppliers,
  insertSupplier,
  updateCategoryById,
  deleteCategoryById,
  searchSuppliers,
  getAllSuppliersNoPage,
};
