const Result = require('folktale/result');
const R = require('ramda');

const uuidv4 = require('../../lib/uuid');
const db = require('../../database/models');

const { Op } = db.Sequelize;

/* eslint-disable */
const loadProducts = async idStore => {
  try {
    const result = await db.sequelize.query(
      `select p.id_product, p.product_name, p.descriptions, p.price, sp.quantity_product, s.supplier_name, c.category_name from ((products p left join stock_products sp on p.id_product = sp.id_product) join suppliers s on s.id_supplier = p.id_supplier) join categories c on c.id_category = p.id_category where sp.id_store = '${idStore}'`,
      {
        type: db.sequelize.QueryTypes.SELECT,
      }
    );
    //console.log(result);
    return Promise.resolve(Result.Ok(result));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

const countOfProductFollowStore = async idStore => {
  try {
    const result = await db.stockProducts.findAndCountAll({
      where: {
        id_store: idStore,
      },
    });
    //console.log(result);
    return Promise.resolve(Result.Ok(result.count));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

const deleteProduct = async id => {
  try {
    const result = await db.products.destroy({
      where: {
        id_product: id,
      },
    });
    return Promise.resolve(Result.Ok(result));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

const editProduct = async ({
  id_product,
  product_name,
  descriptions,
  price,
  img_1,
  img_2,
  id_supplier,
  id_category,
}) => {
  try {
    const result = await db.products.update(
      {
        id_product: id_product,
        product_name: product_name,
        descriptions: descriptions,
        price: price,
        img_1: img_1,
        img_2: img_2,
        id_supplier: id_supplier,
        id_category: id_category,
      },
      {
        where: {
          id_product: id_product,
        },
      }
    );
    return Promise.resolve(Result.Ok(result));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

const addProduct = async data => {
  try {
    const product = await db.products.create({
      ...data,
    });

    return Promise.resolve(Result.Ok(product));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

const addProductInStock = async data => {
  try {
    const result = await db.stockProducts.create({
      ...data,
    });

    return Promise.resolve(Result.Ok(result));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

const searchProductsAllStore = async idProduct => {
  try {
    const result = await db.stores.findAll({
      include: [
        {
          model: db.stockProducts,
          where: {
            id_product: idProduct,
          },
        },
      ],
    });

    return Promise.resolve(Result.Ok(result));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

const insertRequestVote = async (totalProduct, idUser) => {
  try {
    const now = new Date();

    const time = `${now.getFullYear()}-${now.getMonth() +
      1}-${now.getDate()} ${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}.${now.getMilliseconds()}`;

    const result = await db.requestVotes.create({
      id_request: uuidv4.uuid(),
      total_product: totalProduct,
      id_user: idUser,
      id_supplier: uuidv4.uuid(),
      request_date: time,
    });

    return Promise.resolve(Result.Ok(result.id_user));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

const insertInfoRequestDetailVote = async (arrayData, id_request) => {
  try {
    const handlePromise = [];

    arrayData.forEach(item => {
      const now = new Date();

      const time = `${now.getFullYear()}-${now.getMonth() +
        1}-${now.getDate()} ${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}.${now.getMilliseconds()}`;

      handlePromise.push(
        db.requestDetail.create({
          id_request,
          id_request_detail: uuidv4.uuid(),
          id_product: item.id_product,
          quantity_request: item.quantity_product,
          add_at: time,
        })
      );
    });

    const result = await Promise.all(handlePromise);

    return Promise.resolve(Result.Ok(result));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

const searchProductFollowName = async (idStore, nameProduct, perPage, page) => {
  try {
    const products = await db.stockProducts.findAndCountAll({
      include: [
        {
          model: db.products,
          include: [
            {
              model: db.categories,
            },
            {
              model: db.suppliers,
            },
          ],
          where: {
            product_name: {
              [Op.like]: `%${nameProduct}%`,
            },
          },
        },
      ],
      where: {
        id_store: idStore,
      },
      offset: (page - 1) * parseInt(perPage),
      limit: parseInt(perPage),
    });

    products.rows = products.rows.map(item => {
      return {
        ...R.path(['dataValues', 'product', 'dataValues'])(item),
        category_name: R.path([
          'dataValues',
          'product',
          'category',
          'dataValues',
          'category_name',
        ])(item),
        supplier_name: R.path([
          'dataValues',
          'product',
          'supplier',
          'dataValues',
          'supplier_name',
        ])(item),
      };
    });

    return Promise.resolve(Result.Ok(products));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

module.exports = {
  loadProducts,
  deleteProduct,
  editProduct,
  addProduct,
  addProductInStock,
  searchProductsAllStore,
  insertInfoRequestDetailVote,
  insertRequestVote,
  countOfProductFollowStore,
  searchProductFollowName,
};
