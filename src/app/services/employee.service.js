const Result = require('folktale/result');
const R = require('ramda');

const db = require('../../database/models');

const loadEmployee = async idManager => {
  try {
    const result = await db.users.findAll({
      where: {
        id_manager: idManager,
      },
      attributes: {
        exclude: ['password'],
      },
    });

    const returnResult = result.map(employ => {
      return {
        ...R.path(['dataValues'])(employ),
      };
    });
    return Promise.resolve(Result.Ok(returnResult));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

const createEmployee = async ({
  id_user,
  display_name,
  user_name,
  email,
  password,
  gender,
  address,
  is_actived,
  verified_code,
  id_store,
  id_manager,
}) => {
  try {
    const result = await db.sequelize.query(
      'call CreateUser(:xid_user,:xdisplay_name,:xuser_name,:xemail,:xpassword,:xgender,:xaddress,:xis_actived,:xverified_code,:xid_store,:xid_manager)',
      {
        replacements: {
          xid_user: id_user,
          xdisplay_name: display_name,
          xuser_name: user_name,
          xemail: email,
          xpassword: password,
          xgender: gender,
          xaddress: address,
          xis_actived: is_actived,
          xverified_code: verified_code,
          xid_store: id_store,
          xid_manager: id_manager,
        },
        type: db.sequelize.QueryTypes.INSERT,
      }
    );
    return Promise.resolve(Result.Ok(result));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

const deleteEmployee = async id => {
  try {
    const result = await db.users.destroy({
      where: {
        id_user: id,
      },
    });
    return Promise.resolve(Result.Ok(result));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};
module.exports = {
  loadEmployee,
  createEmployee,
  deleteEmployee,
};
