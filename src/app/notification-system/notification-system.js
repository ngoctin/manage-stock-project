const socket = require('../../core/socket.io.core');

module.exports = async httpServer => {
  // create connection of socket.io
  const io = await socket.socketIOConfig(httpServer);

  /* eslint-disable-next-line no-shadow */
  io.on('connection', socket => {
    socket.on('emit-info', message => {
      console.log(message); /* eslint-disable-line */
    });

    /* eslint-disable-next-line no-console */
    console.log('A user has connected to server !', socket.id);
    socket.on('disconnect', () => {
      /* eslint-disable-next-line no-console */
      console.log('A user has disconnected to server !', socket.id);
    });
  });
};
