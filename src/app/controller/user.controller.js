const db = require('../../database/models');
const uuidHelper = require('../../lib/uuid');
const userService = require('../services/user.service');

const renderLogin = (req, res, next) => {
  try {
    return res.render('auth/login', {
      layout: false,
    });
  } catch (error) {
    return next(error);
  }
};

const renderFirstLogin = (req, res, next) => {
  try {
    return res.render('auth/first_login', {
      layout: false,
    });
  } catch (error) {
    return next(error);
  }
};

const renderError404 = (req, res, next) => {
  try {
    return res.render('error-page/error404', {
      layout: false,
    });
  } catch (error) {
    return next(error);
  }
};

const renderError500 = (req, res, next) => {
  try {
    return res.render('error-page/error500', {
      layout: false,
    });
  } catch (error) {
    return next(error);
  }
};

const renderSettings = (req, res, next) => {
  try {
    return res.render('setting/setting');
  } catch (error) {
    return next(error);
  }
};

const renderChangePass = (req, res, next) => {
  try {
    return res.render('setting/change-pass');
  } catch (error) {
    return next(error);
  }
};

const renderLogout = (req, res, next) => {
  try {
    return res.render('auth/login', {
      layout: false,
    });
  } catch (error) {
    return next(error);
  }
};

module.exports = {
  renderLogin,
  renderFirstLogin,
  renderError404,
  renderError500,
  renderSettings,
  renderChangePass,
  renderLogout,
};
