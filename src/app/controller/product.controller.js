const config = require('config');

const uuidv4 = require('../../lib/uuid');

const productService = require('../services/product.service');
const categoryService = require('../services/category.service');
const supplierService = require('../services/supplier.service');

const renderProducts = async (req, res, next) => {
  try {
    const { page = 1 } = req.query;

    const result = await productService.loadProducts(req.user.id_store);

    if (result.value instanceof Error) throw result.value;

    const count = await productService.countOfProductFollowStore(
      req.user.id_store
    );

    if (count.value instanceof Error) throw count.value;

    const message = req.session.createProduct;
    req.session.createProduct = null;

    return res.render('manage-stock/admin/product/manager-products.hbs', {
      products: result.value,
      success: message,
      message,
      role: req.user.roles[0],
      pagination: {
        /* eslint-disable radix */
        currentPage: page,
        hasNextPage:
          parseInt(page) * config.get('CONFIGURATION.PER_PAGE') < count.value,
        hasPreviousPage: parseInt(page) > 1,
        nextPage: parseInt(page) + 1,
        previousPage: parseInt(page) - 1,
        currentUrl: `${
          req.originalUrl.toString().match(/\page=[0-9]+/gm) === ''
            ? `${req.path}/${req.originalUrl
                .toString()
                .replace(/\page=[0-9]+/, 'page=')}`
            : `${req.path}?page=`
        }`,
      },
    });
  } catch (error) {
    return next(error);
  }
};

const deleteProduct = async (req, res, next) => {
  try {
    console.log(req.params.id);
    await productService.deleteProduct(req.params.id);
    return res.redirect('/manage-products');
  } catch (error) {
    return next(error);
  }
};

const renderViewAddProduct = async (req, res, next) => {
  try {
    const categories = await categoryService.loadAllCatogoriesNoPage();
    const suppliers = await supplierService.getAllSuppliersNoPage();

    if (categories.value instanceof Error) throw categories.value;
    if (suppliers.value instanceof Error) throw suppliers.value;

    return res.render('manage-stock/admin/product/add-product', {
      title: 'Thêm sản phẩm mới',
      categories: categories.value,
      suppliers: suppliers.value,
      store: req.user.id_store,
    });
  } catch (error) {
    return next(error);
  }
};

const addAProduct = async (req, res, next) => {
  try {
    const {
      nameProduct,
      descriptionProduct,
      priceProduct,
      categoryProduct,
      supplierProduct,
      amountProduct,
      storeProduct,
    } = req.body;

    const {
      filename: filename1,
      destination: destination1,
    } = req.files.img1Product[0];

    const {
      filename: filename2,
      destination: destination2,
    } = req.files.img2Product[0];

    const product = await productService.addProduct({
      id_product: uuidv4.uuid(),
      product_name: nameProduct,
      descriptions: descriptionProduct,
      price: priceProduct,
      id_category: categoryProduct,
      id_supplier: supplierProduct,
      img_1: `/static/upload/${
        destination1.toString().split('/')[
          destination1.toString().split('/').length - 1
        ]
      }/${filename1}`, // eslint-disable-line
      img_2: `/static/upload/${
        destination2.toString().split('/')[
          destination2.toString().split('/').length - 1
        ]
      }/${filename2}`,
    });

    if (product.value instanceof Error) throw product.value;

    const stock = await productService.addProductInStock({
      id_pro_stk: uuidv4.uuid(),
      id_product: product.value.id_product,
      id_store: storeProduct,
      quantity_product: amountProduct,
    });

    if (stock.value instanceof Error) throw stock.value;

    req.session.createProduct = 'Tạo sản phẩm thành công';

    return res.redirect('/manage-products');
  } catch (error) {
    return next(error);
  }
};

const productInStores = async (req, res, next) => {
  try {
    const { id_product } = req.params;

    const result = await productService.searchProductsAllStore(id_product);

    if (result.value instanceof Error)
      return res.status(400).json({
        status: 400,
        code: 'USR_02',
        message: 'bad request',
      });

    return res.status(200).json({
      status: 200,
      code: 'USR_03',
      result: result.value,
    });
  } catch (error) {
    return res.status(500).json({
      status: 500,
      code: 'USR_01',
      message: 'internal server error',
    });
  }
};

const renderMakeRequestVote = async (req, res, next) => {
  try {
    const products = await productService.loadProducts(req.user.id_store);

    if (products.value instanceof Error) throw products.value;

    const message = req.session.createSuccess;
    req.session.createSuccess = null;

    return res.render('manage-stock/admin/product/make-request-vote', {
      products: products.value,
      bills: req.session.requestBill,
      success: message,
      message,
    });
  } catch (error) {
    return next(error);
  }
};

const initializeSessionForMakeVotes = async (req, res, next) => {
  try {
    /* eslint-disable-next-line camelcase */
    const { id_product, name_product, quantity_product } = req.body;

    if (!req.session.requestBill) {
      req.session.requestBill = [];
    }

    // eslint-disable-next-line
    let product = req.session.requestBill.find(
      /* eslint-disable-next-line camelcase */
      item => item.id_product === id_product
    );

    if (product) {
      product.quantity_product += quantity_product;
    } else {
      req.session.requestBill.push({
        id_product,
        name_product,
        quantity_product,
      });
    }

    return res.status(200).json({
      status: 200,
      code: 'USR_03',
      result: req.session.requestBill,
    });
  } catch (error) {
    return res.status(500).json({
      status: 500,
      code: 'USR_01',
      message: 'internal server error',
    });
  }
};

const deleteProductInSession = async (req, res, next) => {
  try {
    /* eslint-disable-next-line camelcase */
    const { id_product_deleted } = req.body;

    const index = req.session.requestBill.findIndex(
      product => product.id_product === id_product_deleted
    );
    req.session.requestBill.splice(index, 1);

    return res.status(200).json({
      status: 200,
      code: 'USR_03',
      result: req.session.requestBill,
    });
  } catch (error) {
    return res.status(500).json({
      status: 500,
      code: 'USR_01',
      message: 'internal server error',
    });
  }
};

const createRequestVote = async (req, res, next) => {
  try {
    const total = req.session.requestBill.reduce(
      (init, product) => init + product.quantity_product,
      0
    );

    const requestVote = await productService.insertRequestVote(
      total,
      req.user.id_user
    );

    if (requestVote.value instanceof Error) throw requestVote.value;

    const result = await productService.insertInfoRequestDetailVote(
      req.session.requestBill,
      requestVote.value
    );

    if (result.value instanceof Error) throw result.value;

    req.session.requestBill = null;

    req.session.createSuccess = 'Lập phiếu thành công';

    return res.redirect('/create-request-bill');
  } catch (error) {
    return next(error);
  }
};

const searchProductsFollowName = async (req, res, next) => {
  try {
    const { show, query_string, page = 1 } = req.query;

    const products = await productService.searchProductFollowName(
      req.user.id_store,
      query_string,
      show,
      page
    );

    if (products.value instanceof Error) throw products.value;

    return res.render('manage-stock/admin/product/manager-products', {
      products: products.value.rows,
      pagination: {
        /* eslint-disable */
        currentPage: page,
        hasNextPage: parseInt(page) * show < products.value.count,
        hasPreviousPage: parseInt(page) > 1,
        nextPage: parseInt(page) + 1,
        previousPage: parseInt(page) - 1,
        currentUrl: `${req.originalUrl.replace(/&page=[0-9]+/, '')}&page=`,
      },
    });
  } catch (error) {
    return next(error);
  }
};

module.exports = {
  renderProducts,
  deleteProduct,
  renderViewAddProduct,
  addAProduct,
  productInStores,
  renderMakeRequestVote,
  initializeSessionForMakeVotes,
  deleteProductInSession,
  createRequestVote,
  searchProductsFollowName,
};
