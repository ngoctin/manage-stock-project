const renderState = (req, res, next) => {
  try {
    return res.render('manage-statement/statement');
  } catch (error) {
    return next(error);
  }
};

module.exports = {
  renderState,
};
