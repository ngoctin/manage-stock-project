const orderService = require('../services/order.service');
const uuidHelper = require('../../lib/uuid');
const db = require('../../database/models');

const renderOrders = async (req, res, next) => {
  try {
    const result = await orderService.loadOrders();
    return res.render('manage-stock/admin/order/manage-orders', {
      orders: result.value,
    });
  } catch (error) {
    return next(error);
  }
};

const renderOrderDetail = async (req, res, next) => {
  try {
    const result = await orderService.loadOrderDetail(req.params.id);
    // console.log(result);
    // eslint-disable-next-line prefer-destructuring
    const id_order = result.value[0].id_order;
    // eslint-disable-next-line prefer-destructuring
    const percentage_tax = result.value[0].percentage_tax;
    // eslint-disable-next-line prefer-destructuring
    const total_price = result.value[0].total_price;
    for (let i = 0; i < result.value.length; i += 1) {
      delete result.value[i].id_order;
      delete result.value[i].percentage_tax;
      delete result.value[i].total_price;
      result.value[i].stt = i + 1;
    }
    console.log(result.value);
    return res.render('manage-stock/admin/order/order-detail', {
      rows: result.value,
      id_order,
      percentage_tax,
      total_price,
    });
  } catch (error) {
    return next(error);
  }
};

const renderCreateOrder = (req, res, next) => {
  try {
    return res.render('manage-stock/admin/order/order.hbs');
  } catch (error) {
    return next(error);
  }
};

const createOrder = async (req, res, next) => {
  try {
    // đợi view để điền tham số vào
    const result = await orderService.createOrder({
      id_order: uuidHelper.uuid(),
      create_on: req.body.user_name,
      total_product: req.body.user_name,
      total_price: req.body.user_name,
      id_tax: req.body.user_name,
      id_customer: req.body.user_name,
      id_user: req.body.user_name,
    });
    if (result.value instanceof Error) throw result.value;
    return res.render('manage-orders/create-order');
  } catch (error) {
    return next(error);
  }
};

module.exports = {
  renderOrders,
  renderOrderDetail,
  renderCreateOrder,
  createOrder,
};
