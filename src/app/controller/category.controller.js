const config = require('config');

const categoryService = require('../services/category.service');
const uuidHelper = require('../../lib/uuid');

/**
 *
 * @param {*} req Request is comming
 * @param {*} res Response
 * @param {*} next Middleware
 */
const renderCategories = async (req, res, next) => {
  try {
    const { page = 1 } = req.query;

    const categories = await categoryService.loadAllCatogories(page);

    if (categories.value instanceof Error) throw categories.value;

    const successVal = req.session.success;
    const messageVal = req.session.message;

    req.session.success = null;
    req.session.message = null;

    return res.render('manage-stock/admin/category/categories.hbs', {
      categories: categories.value.rows,
      success: successVal,
      message: messageVal,
      pagination: {
        /* eslint-disable radix */
        currentPage: page,
        hasNextPage:
          parseInt(page) * config.get('CONFIGURATION.PER_PAGE') <
          categories.value.count,
        hasPreviousPage: parseInt(page) > 1,
        nextPage: parseInt(page) + 1,
        previousPage: parseInt(page) - 1,
        currentUrl: `${
          req.originalUrl.toString().match(/\page=[0-9]+/gm) === ''
            ? `${req.path}/${req.originalUrl
                .toString()
                .replace(/\page=[0-9]+/, 'page=')}`
            : `${req.path}?page=`
        }`,
      },
    });
  } catch (error) {
    return next(error);
  }
};

const insertACategory = async (req, res, next) => {
  try {
    const { categoryName, description, page = 1 } = req.body;

    const id = uuidHelper.uuid();

    const result = await categoryService.insertACategory({
      idCategory: id,
      categoryName,
      description,
    });

    if (result.value instanceof Error) throw result.value;

    const categories = await categoryService.loadAllCatogories(page);

    if (categories.value instanceof Error) throw categories.value;

    req.session.success = true;
    req.session.message = 'Thêm thành công';

    return res.redirect(req.session.previousUrl);
  } catch (error) {
    return next(error);
  }
};

const searchCategories = async (req, res, next) => {
  try {
    /* eslint-disable-next-line camelcase */
    const { show_number, query_string, page = 1 } = req.query;

    const categoriesSearch = await categoryService.searchCategoriesFollowName(
      show_number,
      query_string,
      page
    );

    if (categoriesSearch.value instanceof Error) throw categoriesSearch.value;

    return res.render('manage-stock/admin/category/categories', {
      categories: categoriesSearch.value.rows,
      pagination: {
        /* eslint-disable */
        currentPage: page,
        hasNextPage:
          parseInt(page) * show_number < categoriesSearch.value.count,
        hasPreviousPage: parseInt(page) > 1,
        nextPage: parseInt(page) + 1,
        previousPage: parseInt(page) - 1,
        currentUrl: `${req.originalUrl.replace(/&page=[0-9]+/, '')}&page=`,
      },
    });
  } catch (error) {
    return next(error);
  }
};

const updateACategory = async (req, res, next) => {
  try {
    const { idCategory, categoryName, description } = req.body;
    const result = await categoryService.updateACategory(
      idCategory,
      categoryName,
      description
    );

    if (result.value instanceof Error)
      return res.status(400).json({
        status: 400,
        code: 'USR_02',
        message: 'bad request',
      });

    return res.status(200).json({
      status: 200,
      code: 'USR_03',
      result: result.value,
    });
  } catch (error) {
    return res.status(500).json({
      status: 500,
      code: 'USR_01',
      message: 'internal server error',
    });
  }
};

const deleteCategory = async (req, res, next) => {
  try {
    const { idCategory } = req.body;

    const result = await categoryService.deleteACategory(idCategory);

    if (result.value instanceof Error)
      return res.status(400).json({
        status: 400,
        code: 'USR_02',
        message: 'bad request',
      });

    return res.status(200).json({
      status: 200,
      code: 'USR_03',
      result: result.value,
    });
  } catch (error) {
    return res.status(500).json({
      status: 500,
      code: 'USR_01',
      message: 'internal server error',
    });
  }
};

module.exports = {
  renderCategories,
  insertACategory,
  searchCategories,
  updateACategory,
  deleteCategory,
};
