const employeeService = require('../services/employee.service.js');
const db = require('../../database/models');
const uuidHelper = require('../../lib/uuid');

const renderEmployee = async (req, res, next) => {
  try {
    const result = await employeeService.loadEmployee();
    // console.log(result);
    return res.render('manage-stock/admin/employee/manage-employees', {
      rows: result.value,
    });
  } catch (error) {
    return next(error);
  }
};

const deleteEmployee = async (req, res, next) => {
  try {
    await employeeService.deleteEmployee(req.params.id);
    // console.log(result);
    return res.redirect('/manage-employees');
  } catch (error) {
    return next(error);
  }
};
module.exports = {
  renderEmployee,
  deleteEmployee,
};
