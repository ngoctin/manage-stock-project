const userService = require('../services/user.service');
const employeeService = require('../services/employee.service.js');
const uuidHelper = require('../../lib/uuid');
const globalVariables = require('../../lib/globalVariables');

const renderInsertAccount = async (req, res, next) => {
  try {
    return res.render('manage-stock/admin/account/insert-account');
  } catch (error) {
    return next(error);
  }
};

const insertAccount = async (req, res, next) => {
  const id_user = uuidHelper.uuid();
  try {
    await employeeService.createEmployee({
      id_user,
      display_name: req.body.display_name,
      user_name: req.body.user_name,
      email: req.body.email,
      password: req.body.password,
      gender: req.body.gender,
      address: req.body.address,
      is_actived: false,
      verified_code: '123',
      id_store: req.body.id_store,
      id_manager: '123',
    });
    await userService.insertUserRole({
      id_role: req.body.id_role,
      id_user,
    });
    //    if (result.value instanceof Error) throw result.value;
    return res.render('manage-stock/admin/account/insert-account');
  } catch (error) {
    return next(error);
  }
};

const renderUpdateAccount = (req, res, next) => {
  try {
    return res.render('manage-stock/admin/account/update-account');
  } catch (error) {
    return next(error);
  }
};

const renderUpdatePassword = (req, res, next) => {
  try {
    return res.render('manage-stock/admin/account/update-password');
  } catch (error) {
    return next(error);
  }
};

const renderLogin = (req, res, next) => {
  try {
    return res.render('manage-stock/admin/login', {
      layout: false,
    });
  } catch (error) {
    return next(error);
  }
};

const login = async (req, res, next) => {
  try {
    const { email, password } = req.body;

    const user = await userService.findUserByEmail(email);

    if (user.value instanceof Error) throw user.value;

    if (!user.value)
      return res.render('manage-stock/admin/login', {
        status: true,
        error: 'Email không tồn tại',
        layout: false,
      });

    const checked = await user.value.comparePassword(password);

    if (!checked)
      return res.render('manage-stock/admin/login', {
        layout: false,
        status: true,
        error: 'Mật khẩu chính xác',
      });

    const role = await userService.findUserWithRoleById(user.value.id_user);

    if (role.value instanceof Error) throw role.value;

    let obj = null; // eslint-disable-line

    if (role.value[0] === 'STAFF') {
      obj = globalVariables.listItemOfStaff.find(item => item.role === 'STAFF');
    } else if (role.value[0] === 'MANAGER') {
      obj = globalVariables.listItemOfManager.find(
        item => item.role === 'MANAGER'
      );
    } else {
      obj = globalVariables.listItemOfAdmin.find(item => item.role === 'ADMIN');
    }

    req.session.returnUrl = obj.router;

    res.cookie('managestockSession', user.value.id_user, {
      signed: true,
      httpOnly: true,
      maxAge: 1 * 60 * 60 * 1000, // exist in 1h
    });

    return res.redirect(obj.router);
  } catch (error) {
    return next(error);
  }
};

const logout = async (req, res, next) => {
  try {
    res.clearCookie('managestockSession');

    return res.redirect('/login-account');
  } catch (error) {
    return next(error);
  }
};

module.exports = {
  renderInsertAccount,
  renderUpdateAccount,
  renderUpdatePassword,
  renderLogin,
  login,
  insertAccount,
  logout,
};
