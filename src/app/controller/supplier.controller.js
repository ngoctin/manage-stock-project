const config = require('config');

const uuidv4 = require('../../lib/uuid');
const supplierService = require('../services/supplier.service');

const renderSuppliers = async (req, res, next) => {
  try {
    const { page = 1 } = req.query;

    /* eslint-disable-next-line */
    const suppliers = await supplierService.getAllSuppliers(parseInt(page));

    if (suppliers.value instanceof Error) throw suppliers.value;

    const successVal = req.session.success;
    const messageVal = req.session.message;

    req.session.success = null;
    req.session.message = null;

    return res.render('manage-stock/admin/supplier/suppliers', {
      suppliers: suppliers.value.rows,
      success: successVal,
      message: messageVal,
      pagination: {
        /* eslint-disable radix */
        currentPage: page,
        hasNextPage:
          parseInt(page) * config.get('CONFIGURATION.PER_PAGE') <
          suppliers.value.count,
        hasPreviousPage: parseInt(page) > 1,
        nextPage: parseInt(page) + 1,
        previousPage: parseInt(page) - 1,
        currentUrl: `${
          req.originalUrl.toString().match(/\page=[0-9]+/gm) === ''
            ? `${req.path}/${req.originalUrl
                .toString()
                .replace(/\page=[0-9]+/, 'page=')}`
            : `${req.path}?page=`
        }`,
      },
    });
  } catch (error) {
    return next(error);
  }
};

const insertASupplier = async (req, res, next) => {
  try {
    const { nameSupplier, addressSupplier, phoneSupplier } = req.body;

    const supplier = await supplierService.insertSupplier({
      id_supplier: uuidv4.uuid(),
      supplier_name: nameSupplier,
      address: addressSupplier,
      phone: phoneSupplier,
    });

    if (supplier.value instanceof Error) throw supplier.value;

    req.session.success = true;
    req.session.message = 'Thêm thành công';

    return res.redirect(req.session.previousUrl);
  } catch (error) {
    return next(error);
  }
};

/* eslint-disable-next-line no-unused-vars */
const updateACategory = async (req, res, next) => {
  try {
    const {
      idSupplier,
      nameSupplier,
      addressSupplier,
      phoneNumberSupplier,
    } = req.body;

    const result = await supplierService.updateCategoryById(idSupplier, {
      supplier_name: nameSupplier,
      address: addressSupplier,
      phone: phoneNumberSupplier,
    });

    if (result.value instanceof Error)
      return res.status(400).json({
        status: 400,
        code: 'USR_02',
        message: 'bad request',
      });

    return res.status(200).json({
      status: 200,
      code: 'USR_03',
      result: result.value,
    });
  } catch (error) {
    return res.status(500).json({
      status: 500,
      code: 'USR_01',
      message: 'internal server error',
    });
  }
};

/* eslint-disable-next-line no-unused-vars */
const deleteSupplier = async (req, res, next) => {
  try {
    const { idSupplier } = req.body;

    const result = await supplierService.deleteCategoryById(idSupplier);

    if (result.value instanceof Error)
      return res.status(400).json({
        status: 400,
        code: 'USR_02',
        message: 'bad request',
      });

    return res.status(200).json({
      status: 200,
      code: 'USR_03',
      result: result.value,
    });
  } catch (error) {
    return res.status(500).json({
      status: 500,
      code: 'USR_01',
      message: 'internal server error',
    });
  }
};

const searchAllSuppliers = async (req, res, next) => {
  try {
    const { query_string, show, page = 1 } = req.query; // eslint-disable-line

    const suppliers = await supplierService.searchSuppliers(
      query_string,
      show,
      page
    );

    if (suppliers.value instanceof Error) throw suppliers.value;

    return res.render('manage-stock/admin/supplier/suppliers', {
      suppliers: suppliers.value.rows,
      pagination: {
        /* eslint-disable radix */
        currentPage: page,
        hasNextPage: parseInt(page) * show < suppliers.value.count,
        hasPreviousPage: parseInt(page) > 1,
        nextPage: parseInt(page) + 1,
        previousPage: parseInt(page) - 1,
        currentUrl: `${req.originalUrl.replace(/&page=[0-9]+/, '')}&page=`,
      },
    });
  } catch (error) {
    return next(error);
  }
};

module.exports = {
  renderSuppliers,
  insertASupplier,
  updateACategory,
  deleteSupplier,
  searchAllSuppliers,
};
