const renderCustomers = (req, res, next) => {
  try {
    return res.render('manage-stock/admin/customer/customers.hbs');
  } catch (error) {
    return next(error);
  }
};

module.exports = {
  renderCustomers,
};
