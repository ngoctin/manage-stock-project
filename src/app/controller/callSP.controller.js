const db = require('../../database/models');

module.exports.callSP = async (req, res, next) => {
  try {
    const result = await db.sequelize.query(
      'CALL catalog_add_attribute(:inName)',
      {
        replacements: { inName: 'Weight' },
        type: db.sequelize.QueryTypes.INSERT,
      }
    );

    return res.status(201).json({
      result,
    });
  } catch (error) {
    return next(error);
  }
};
