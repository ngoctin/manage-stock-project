const renderStatistical = (req, res, next) => {
  try {
    return res.render('manage-stock/admin/statistical/statisticals');
  } catch (error) {
    return next(error);
  }
};

module.exports = {
  renderStatistical,
};
