module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ordersDetails', {
      id_order: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.STRING(50),
      },

      id_order_detail: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
      created_date: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      quatity_order: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      unit_cost: {
        type: Sequelize.DECIMAL,
        allowNull: false,
      },
      line_total: {
        type: Sequelize.DECIMAL,
        allowNull: false,
      },
      id_product: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('ordersDetails');
  },
};
