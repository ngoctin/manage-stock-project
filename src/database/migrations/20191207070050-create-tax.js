module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('taxes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.STRING(50),
      },
      percentage_tax: {
        type: Sequelize.DECIMAL,
        allowNull: false,
      },
      type_tax: {
        type: Sequelize.STRING(100),
        alllowNull: false,
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('taxes');
  },
};
