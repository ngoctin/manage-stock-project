module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('users', {
      id_user: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.STRING(50),
      },
      display_name: {
        type: Sequelize.STRING(100),
        allowNull: false,
      },
      user_name: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
      email: {
        type: Sequelize.STRING(100),
      },
      password: {
        type: Sequelize.STRING(100),
      },
      gender: {
        type: Sequelize.STRING(7),
      },
      address: {
        type: Sequelize.STRING(150),
        allowNull: false,
      },
      is_actived: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      verified_code: {
        type: Sequelize.STRING(30),
        allowNull: false,
      },
      id_store: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
      id_manager: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('users');
  },
};
