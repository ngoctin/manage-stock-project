module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('orders', {
      id_order: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.STRING(50),
      },
      create_on: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      total_product: {
        type: Sequelize.INTERGER,
        allowNull: false,
      },
      total_price: {
        type: Sequelize.DECIMAL,
        allowNull: false,
      },
      id_tax: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
      id_customer: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
      id_user: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('orders');
  },
};
