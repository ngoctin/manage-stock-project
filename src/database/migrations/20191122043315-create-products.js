module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('products', {
      id_product: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.STRING(50),
      },

      product_name: {
        type: Sequelize.STRING(100),
        allowNull: false,
      },
      descriptions: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      price: {
        type: Sequelize.DECIMAL,
        allowNull: false,
      },
      img_1: {
        type: Sequelize.STRING(100),
        allowNull: false,
      },
      img2: {
        type: Sequelize.STRING(100),
        allowNull: false,
      },
      id_supplier: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
      id_category: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('products');
  },
};
