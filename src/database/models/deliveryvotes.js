module.exports = (sequelize, DataTypes) => {
  const deliveryVotes = sequelize.define(
    'deliveryVotes',
    {
      id_delivery: {
        type: DataTypes.STRING(50),
        primaryKey: true,
      },
      delivery_date: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      total_product: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      total_price: {
        type: DataTypes.DECIMAL(16, 0),
        allowNull: true,
        defaultValue: 0.0,
      },
      id_request: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
    },
    {
      tableName: 'delivery_votes',
      timestamps: false,
    }
  );
  deliveryVotes.associate = function({ requestVotes, deliveryDetail }) {
    // associations can be defined here
    deliveryVotes.belongsTo(requestVotes, {
      foreignKey: 'id_request',
      targetKey: 'id_request',
    });
    deliveryVotes.hasMany(deliveryDetail, {
      foreignKey: 'id_delivery',
      sourceKey: 'id_delivery',
    });
  };
  return deliveryVotes;
};
