module.exports = (sequelize, DataTypes) => {
  const stockProducts = sequelize.define(
    'stockProducts',
    {
      id_pro_stk: {
        type: DataTypes.STRING(50),
        primaryKey: true,
      },
      quantity_product: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      id_store: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
      id_product: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
    },
    {
      tableName: 'stock_products',
      timestamps: false,
    }
  );
  stockProducts.associate = function({ products, stores }) {
    // associations can be defined here
    stockProducts.belongsTo(stores, {
      foreignKey: 'id_store',
      targetKey: 'id_store',
    });
    stockProducts.belongsTo(products, {
      foreignKey: 'id_product',
      targetKey: 'id_product',
    });
  };
  return stockProducts;
};
