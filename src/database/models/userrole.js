module.exports = (sequelize, DataTypes) => {
  const userRole = sequelize.define(
    'user_role',
    {
      id_role: {
        type: DataTypes.STRING(50),
        primaryKey: true,
      },
      id_user: {
        type: DataTypes.STRING(50),
        primaryKey: true,
      },
    },
    {
      tableName: 'user_role',
      timestamps: false,
    }
  );
  userRole.associate = function({ roles, users }) {
    // associations can be defined here
    userRole.belongsTo(roles, {
      foreignKey: 'id_role',
      targetKey: 'id_role',
    });
    userRole.belongsTo(users, {
      foreignKey: 'id_user',
      targetKey: 'id_user',
    });
  };
  return userRole;
};
