const bcryptjs = require('bcryptjs');

module.exports = (sequelize, DataTypes) => {
  const users = sequelize.define(
    'users',
    {
      id_user: {
        type: DataTypes.STRING(50),
        primaryKey: true,
      },
      display_name: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      user_name: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      password: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      gender: {
        type: DataTypes.STRING(7),
        allowNull: false,
      },
      address: {
        type: DataTypes.STRING(150),
        allowNull: false,
      },
      is_actived: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      verified_code: {
        type: DataTypes.STRING(30),
        allowNull: false,
      },
      id_store: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
      id_manager: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
    },
    {
      tableName: 'users',
      timestamps: false,
      hooks: {
        beforeCreate: async (user, option) => {
          user.password = await user.hashPassword();
        },
      },
    }
  );

  users.prototype.hashPassword = async function() {
    return bcryptjs.hash(this.password, 10);
  };

  users.prototype.comparePassword = async function(password) {
    return bcryptjs.compare(password, this.password);
  };
  // users.associate = function(models) {
  // associations can be defined here
  // };
  users.associate = function({
    stores,
    roles,
    orders,
    requestVotes,
    permissions,
  }) {
    // associations can be defined here
    users.belongsTo(stores, {
      foreignKey: 'id_store',
      targetKey: 'id_store',
      as: 'user1',
    });
    users.belongsToMany(permissions, {
      through: 'user_permission',
      foreignKey: 'id_user',
      otherKey: 'id_permission',
    });
    users.belongsToMany(roles, {
      through: 'user_role',
      foreignKey: 'id_user',
      otherKey: 'id_role',
    });
    users.hasOne(requestVotes, {
      foreignKey: 'id_user',
      sourceKey: 'id_user',
    });
    users.hasMany(orders, {
      foreignKey: 'id_user',
      sourceKey: 'id_user',
    });
    users.hasOne(stores, {
      foreignKey: 'id_user',
      sourceKey: 'id_user',
    });
  };
  return users;
};
