module.exports = (sequelize, DataTypes) => {
  const ordersDetail = sequelize.define(
    'ordersDetail',
    {
      id_order: {
        type: DataTypes.STRING(50),
        primaryKey: true,
      },
      id_order_detail: {
        type: DataTypes.STRING(50),
        primaryKey: true,
      },
      created_date: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      quatity_order: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      unit_cost: {
        type: DataTypes.DECIMAL,
        allowNull: false,
      },
      line_total: {
        type: DataTypes.DECIMAL,
        allowNull: false,
      },
      id_product: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
    },
    {
      tablename: 'orders_detail',
      timestamps: false,
    }
  );
  ordersDetail.associate = function ({ orders, products }) {
    // associations can be defined here
    ordersDetail.belongsTo(orders, {
      foreignKey: 'id_order',
      targetKey: 'id_order',
    });
    ordersDetail.belongsTo(products, {
      foreignKey: 'id_product',
      targetKey: 'id_product',
    });
  };
  return ordersDetail;
};
