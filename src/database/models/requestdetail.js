module.exports = (sequelize, DataTypes) => {
  const requestDetail = sequelize.define(
    'requestDetail',
    {
      id_request: {
        type: DataTypes.STRING(50),
        primaryKey: true,
      },
      id_request_detail: {
        type: DataTypes.STRING(50),
        primaryKey: true,
      },
      add_at: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      quantity_request: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      id_product: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
    },
    {
      tableName: 'request_detail',
      timestamps: false,
    }
  );
  requestDetail.associate = function({ products, requestVotes }) {
    // associations can be defined here
    requestDetail.belongsTo(products, {
      foreignKey: 'id_product',
      targetKey: 'id_product',
    });
    requestDetail.belongsTo(requestVotes, {
      foreignKey: 'id_request',
      targetKey: 'id_request',
    });
  };
  return requestDetail;
};
