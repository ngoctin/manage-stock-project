module.exports = (sequelize, DataTypes) => {
  const orders = sequelize.define(
    'orders',
    {
      id_order: {
        type: DataTypes.STRING(50),
        primaryKey: true,
      },
      create_on: DataTypes.DATE,
      total_product: DataTypes.INTEGER,
      total_price: DataTypes.DECIMAL,
      id_tax: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
      id_customer: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
      id_user: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
    },
    {
      tableName: 'orders',
      timestamps: false,
    }
  );
  orders.associate = function({
    customers,
    users,
    products,
    ordersDetail,
    tax,
  }) {
    // associations can be defined here
    orders.belongsTo(tax, {
      foreignKey: 'id_tax',
      targetKey: 'id_tax',
    });
    orders.belongsTo(customers, {
      foreignKey: 'id_customer',
      targetKey: 'id_customer',
    });
    orders.belongsTo(users, {
      foreignKey: 'id_user',
      targetKey: 'id_user',
    });
    orders.hasMany(ordersDetail, {
      foreignKey: 'id_order',
      sourceKey: 'id_order',
    });
  };
  return orders;
};
