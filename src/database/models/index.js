/* eslint-disable strict */
/* eslint-disable dot-notation */
/* eslint-disable import/no-dynamic-require */
/* eslint-disable no-path-concat */
/* eslint-disable prefer-template */

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');

const basename = path.basename(__filename);
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../config/config.js')[env];
const logger = require('../../lib/logger');

const db = {};

let sequelize;
if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable]);
} else {
  sequelize = new Sequelize(
    config.database,
    config.username,
    config.password,
    config
  );
}

/* Check connection to database */
sequelize
  .authenticate()
  .then(() => {
    /* eslint-disable-next-line no-console */
    logger.info(`[MYSQL] Connected to database`);
  })
  .catch(err => {
    /* eslint-disable-next-line no-console */
    logger.error(`[MYSQL] Unable connect to database: \n ${err}`);
  });

fs.readdirSync(__dirname)
  .filter(file => {
    return (
      file.indexOf('.') !== 0 && file !== basename && file.slice(-3) === '.js'
    );
  })
  .forEach(file => {
    const model = sequelize['import'](path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
