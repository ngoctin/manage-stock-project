module.exports = (sequelize, DataTypes) => {
  const suppliers = sequelize.define(
    'suppliers',
    {
      id_supplier: {
        type: DataTypes.STRING(50),
        primaryKey: true,
      },
      supplier_name: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      phone: {
        type: DataTypes.STRING(20),
        allowNull: false,
      },
      address: {
        type: DataTypes.STRING(150),
        allowNull: false,
      },
    },
    {
      tableName: 'suppliers',
      timestamps: false,
    }
  );
  suppliers.associate = function({ products, requestVotes }) {
    // associations can be defined here
    suppliers.hasMany(products, {
      foreignKey: 'id_supplier',
      sourceKey: 'id_supplier',
    });
    suppliers.hasMany(requestVotes, {
      foreignKey: 'id_supplier',
      sourceKey: 'id_supplier',
    });
  };
  return suppliers;
};
