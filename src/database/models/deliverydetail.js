module.exports = (sequelize, DataTypes) => {
  const deliveryDetail = sequelize.define(
    'deliveryDetail',
    {
      id_delivery: {
        type: DataTypes.STRING(50),
        primaryKey: true,
      },
      id_delivery_detail: {
        type: DataTypes.STRING(50),
        primaryKey: true,
      },
      quantity_delivery: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      price_item: {
        type: DataTypes.DECIMAL(16, 0),
        allowNull: false,
        defaultValue: 0.0,
      },
      sub_total: {
        type: DataTypes.DECIMAL(16, 0),
        allowNull: false,
      },
      id_product: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
    },
    {
      tableName: 'delivery_detail',
      timestamps: false,
    }
  );
  deliveryDetail.associate = function({ products, deliveryVotes }) {
    // associations can be defined here
    deliveryDetail.belongsTo(products, {
      foreignKey: 'id_product',
      targetKey: 'id_product',
    });
    deliveryDetail.belongsTo(deliveryVotes, {
      foreignKey: 'id_delivery',
      targetKey: 'id_delivery',
    });
  };
  return deliveryDetail;
};
