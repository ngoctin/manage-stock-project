module.exports = (sequelize, DataTypes) => {
  const permissions = sequelize.define(
    'permissions',
    {
      id_permission: {
        type: DataTypes.STRING(50),
        primaryKey: true,
      },
      permission_name: {
        type: DataTypes.STRING(200),
        allowNull: false,
      },
      description: {
        type: DataTypes.STRING(200),
        allowNull: false,
      },
    },
    {
      tableName: 'permissions',
      timestamps: false,
    }
  );
  permissions.associate = function({ users }) {
    // associations can be defined here
    permissions.belongsToMany(users, {
      foreignKey: 'id_permission',
      through: 'user_permission',
      otherKey: 'id_user',
    });
  };
  return permissions;
};
