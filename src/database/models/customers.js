module.exports = (sequelize, DataTypes) => {
  const customers = sequelize.define(
    'customers',
    {
      id_customer: {
        type: DataTypes.STRING(50),
        primaryKey: true,
      },
      first_name: {
        type: DataTypes.STRING(20),
        allowNull: false,
      },
      last_name: {
        type: DataTypes.STRING(20),
        allowNull: false,
      },
      phone: {
        type: DataTypes.STRING(15),
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      address: {
        type: DataTypes.STRING(150),
        allowNull: false,
      },
    },
    {
      tableName: 'customers',
      timestamps: false,
    }
  );
  customers.associate = function({ orders }) {
    // associations can be defined here
    customers.hasMany(orders, {
      foreignKey: 'id_customer',
      sourceKey: 'id_customer',
    });
  };
  return customers;
};
