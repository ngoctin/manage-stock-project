module.exports = (sequelize, DataTypes) => {
  const requestVotes = sequelize.define(
    'requestVotes',
    {
      id_request: {
        type: DataTypes.STRING(50),
        primaryKey: true,
      },
      request_date: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      total_product: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      id_user: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
      id_supplier: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
    },
    {
      tableName: 'request_votes',
      timestamps: false,
    }
  );
  requestVotes.associate = function({
    deliveryVotes,
    requestDetail,
    users,
    suppliers,
  }) {
    // associations can be defined here
    requestVotes.belongsTo(users, {
      foreignKey: 'id_user',
      targetKey: 'id_user',
    });
    requestVotes.belongsTo(suppliers, {
      foreignKey: 'id_supplier',
      targetKey: 'id_supplier',
    });
    requestVotes.hasMany(requestDetail, {
      foreignKey: 'id_request',
      sourceKey: 'id_request',
    });
    requestVotes.hasOne(deliveryVotes, {
      foreignKey: 'id_request',
      sourceKey: 'id_request',
    });
  };
  return requestVotes;
};
