module.exports = (sequelize, DataTypes) => {
  const tax = sequelize.define(
    'tax',
    {
      id_tax: {
        type: DataTypes.STRING(50),
        allowNull: false,
        primaryKey: true,
      },
      percentage_tax: {
        type: DataTypes.DECIMAL,
        allowNull: false,
      },
      type_tax: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
    },
    {
      tableName: 'tax',
      timestamps: false,
    }
  );
  tax.associate = function({ orders }) {
    // associations can be defined here
    tax.hasMany(orders, {
      foreignKey: 'id_tax',
      sourceKey: 'id_tax',
    });
  };
  return tax;
};
