module.exports = (sequelize, DataTypes) => {
  const products = sequelize.define(
    'products',
    {
      id_product: {
        type: DataTypes.STRING(50),
        primaryKey: true,
      },
      product_name: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      descriptions: {
        type: DataTypes.TEXT,
        allowNull: false,
      },
      price: {
        type: DataTypes.DECIMAL,
        allowNull: false,
      },
      img_1: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      img_2: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      id_supplier: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
      id_category: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
    },
    {
      tableName: 'products',
      timestamps: false,
    }
  );
  products.associate = function({
    suppliers,
    ordersDetail,
    categories,
    stockProducts,
    requestDetail,
    deliveryDetail,
  }) {
    // associations can be defined here
    products.belongsTo(suppliers, {
      foreignKey: 'id_supplier',
      targetKey: 'id_supplier',
    });
    products.hasMany(ordersDetail, {
      foreignKey: 'id_product',
      sourceKey: 'id_product',
    });
    products.belongsTo(categories, {
      foreignKey: 'id_category',
      targetKey: 'id_category',
    });
    products.hasMany(stockProducts, {
      foreignKey: 'id_product',
      sourceKey: 'id_product',
    });
    products.hasMany(requestDetail, {
      foreignKey: 'id_product',
      sourceKey: 'id_product',
    });
    products.hasMany(deliveryDetail, {
      foreignKey: 'id_product',
      sourceKey: 'id_product',
    });
  };
  return products;
};
