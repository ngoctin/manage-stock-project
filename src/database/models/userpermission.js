module.exports = (sequelize, DataTypes) => {
  const userPermission = sequelize.define(
    'userPermission',
    {
      id_permission: {
        type: DataTypes.STRING(50),
        primaryKey: true,
      },
      id_user: {
        type: DataTypes.STRING(50),
        primaryKey: true,
      },
    },
    {
      tableName: 'user_permission',
      timestamps: false,
    }
  );
  userPermission.associate = function({ permissions, users }) {
    // associations can be defined here
    userPermission.belongsTo(permissions, {
      foreignKey: 'id_permission',
      targetKey: 'id_permission',
    });
    userPermission.belongsTo(users, {
      foreignKey: 'id_user',
      targetKey: 'id_user',
    });
  };
  return userPermission;
};
