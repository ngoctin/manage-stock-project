module.exports = (sequelize, DataTypes) => {
  const roles = sequelize.define(
    'roles',
    {
      id_role: {
        type: DataTypes.STRING(50),
        primaryKey: true,
      },
      role_name: {
        type: DataTypes.STRING(200),
        allowNull: false,
      },
      description: {
        type: DataTypes.STRING(200),
        allowNull: false,
      },
    },
    {
      tableName: 'roles',
      timestamps: false,
    }
  );
  roles.associate = function({ users }) {
    // associations can be defined here
    roles.belongsToMany(users, {
      foreignKey: 'id_role',
      through: 'user_role',
      otherKey: 'id_user',
    });
  };
  return roles;
};
