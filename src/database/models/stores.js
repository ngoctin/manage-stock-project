module.exports = (sequelize, DataTypes) => {
  const stores = sequelize.define(
    'stores',
    {
      id_store: {
        type: DataTypes.STRING(50),
        primaryKey: true,
      },
      store_name: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      phone: {
        type: DataTypes.STRING(15),
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      address: {
        type: DataTypes.STRING(150),
        allowNull: false,
      },
      id_user: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
    },
    {
      tableName: 'stores',
      timestamps: false,
    }
  );
  stores.associate = function({ users, stockProducts }) {
    // associations can be defined here
    stores.hasMany(users, {
      foreignKey: 'id_store',
      sourceKey: 'id_store',
    });
    stores.belongsTo(users, {
      foreignKey: 'id_user',
      targetKey: 'id_user',
    });
    stores.hasMany(stockProducts, {
      foreignKey: 'id_store',
      sourceKey: 'id_store',
    });
  };
  return stores;
};
