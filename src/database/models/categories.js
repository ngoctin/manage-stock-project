module.exports = (sequelize, DataTypes) => {
  const categories = sequelize.define(
    'categories',
    {
      id_category: {
        type: DataTypes.STRING(50),
        primaryKey: true,
      },
      category_name: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      description: {
        type: DataTypes.STRING(200),
        allowNull: false,
      },
    },
    {
      tableName: 'categories',
      timestamps: false,
    }
  );
  categories.associate = function({ products }) {
    // associations can be defined here
    categories.hasMany(products, {
      foreignKey: 'id_category',
      sourceKey: 'id_category',
      onDelete: 'CASCADE',
    });
  };
  return categories;
};
