const Route = require('route-parser');

// // Authentication
// req.user = user;

// // Authorization: Information of user send to authorization middleware
// const authorization = {
//   subject: {
//     userId: '4aeff-fdaea-eecbd',
//     storeId: '4aeff-fdaea-eecbd',
//   },
//   resource: {
//     path: '/product/create',
//     method: 'POST',
//   },
//   permission: 'PO1',
//   role: 'MANAGER',
// };

// req.authorization = authorization;

// // Get origin url
// req.originUrl;

// // Get method
// req.method;

/* eslint-disable */
const match = pattern => path => {
  const route = new Route(pattern);

  const result = route.match(path);

  if (!result) {
    return false;
  }

  return true;
};

module.exports = [
  {
    name: 'Render create orders view',
    description: 'STAFF can see create orders views',
    target: ({ role, path, method }) =>
      path === '/create-order' && method === 'GET',
    condition: ({ role, path, method }) => role.find(item => item === 'STAFF'),
  },
  {
    name: 'Create a order view',
    description: 'STAFF can create a orders views',
    target: ({ role, path, method }) =>
      path === '/create-order' && method === 'POST',
    condition: ({ role, path, method }) => role.find(item => item === 'STAFF'),
  },
  {
    name: 'Render management orders view',
    description: 'MANAGER can see management orders views',
    target: ({ role, path, method }) =>
      path === '/manage-orders' && method === 'GET',
    condition: ({ role, path, method }) =>
      role.find(item => item === 'MANAGER') ||
      role.find(item => item === 'ADMIN'),
  },
  {
    name: 'Render management products view',
    description: 'MANAGER can see management products view',
    target: ({ role, path, method }) =>
      path === '/manage-products' && method === 'GET',
    condition: ({ role, path, method }) =>
      role.find(item => item === 'MANAGER') ||
      role.find(item => item === 'ADMIN'),
  },
  {
    name: 'Render management categories view',
    description: 'MANAGER can see management categories view',
    target: ({ role, path, method }) =>
      path === '/manage-categories' && method === 'GET',
    condition: ({ role, path, method }) => role.find(item => item === 'ADMIN'),
  },
  {
    name: 'Create a create category',
    description: 'MANAGER can create a category',
    target: ({ role, path, method }) =>
      path === '/manage-categories' && method === 'POST',
    condition: ({ role, path, method }) => role.find(item => item === 'ADMIN'),
  },
  {
    name: 'Render management employees view',
    description: 'MANAGER can see management employees view',
    target: ({ role, path, method }) =>
      path === '/manage-employee' && method === 'GET',
    condition: ({ role, path, method }) =>
      role.find(item => item === 'MANAGER'),
  },
  {
    name: 'Render management customers view',
    description: 'MANAGER can see management customer view',
    target: ({ role, path, method }) =>
      path === '/manage-customers' && method === 'GET',
    condition: ({ role, path, method }) =>
      role.find(item => item === 'MANAGER'),
  },
  {
    name: 'Render management suppliers view',
    description: 'ADMIN can see management suppliers view',
    target: ({ role, path, method }) =>
      path === '/manage-suppliers' && method === 'GET',
    condition: ({ role, path, method }) => role.find(item => item === 'ADMIN'),
  },
  {
    name: 'Create a supplier',
    description: 'ADMIN can see management suppliers view',
    target: ({ role, path, method }) =>
      path === '/manage-suppliers' && method === 'GET',
    condition: ({ role, path, method }) => role.find(item => item === 'ADMIN'),
  },
  {
    name: 'Render add product view',
    description: 'MANAGER can add product view',
    target: ({ role, path, method }) =>
      path === '/manage-products/create' && method === 'GET',
    condition: ({ role, path, method }) =>
      role.find(item => item === 'MANAGER'),
  },
  {
    name: 'Add a product',
    description: 'MANAGER can add a product',
    target: ({ role, path, method }) =>
      path === '/manage-products/create' && method === 'POST',
    condition: ({ role, path, method }) =>
      role.find(item => item === 'MANAGER'),
  },
  {
    name: 'Render create request vote view',
    description: 'MANAGER can see request vote view',
    target: ({ role, path, method }) =>
      path === '/create-request-bill' && method === 'GET',
    condition: ({ role, path, method }) =>
      role.find(item => item === 'MANAGER'),
  },
  {
    name: 'Create a request vote',
    description: 'MANAGER can create a request vote',
    target: ({ role, path, method }) =>
      path === '/create-request-bill' && method === 'POST',
    condition: ({ role, path, method }) =>
      role.find(item => item === 'MANAGER'),
  },
  {
    name: 'Pagination on products view',
    description: 'MANAGER can see next page and previous page',
    target: ({ role, path, method }) =>
      match('/manage-products?page=:page')(path) && method === 'GET',
    condition: ({ role, path, method }) =>
      role.find(item => item === 'MANAGER'),
  },
  {
    name: 'Search products',
    description: 'MANAGER can search products',
    target: ({ role, path, method }) =>
      match('/manage-products/search')(path) && method === 'GET',
    condition: ({ role, path, method }) =>
      role.find(item => item === 'MANAGER'),
  },
  {
    name: 'Default',
    description: 'Default',
    target: ({ role, path, method }) => true,
    condition: ({ role, path, method }) => true,
  },
];
