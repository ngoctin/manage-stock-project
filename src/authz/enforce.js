const policies = require('./policy');

/* eslint-disable-next-line */
const matchPolicy = arrPolicies => ({ role, path, method }) => {
  // eslint-disable-next-line
  for (let index = 0; index < arrPolicies.length; index++) {
    if (arrPolicies[index].target({ role, path, method })) {
      return arrPolicies[index];
    }
  }
};

const enforce = ({ role, path, method }) => {
  const policy = matchPolicy(policies)({ role, path, method });
  const result = policy.condition({ role, path, method });
  return result;
};

module.exports = {
  enforce,
};
