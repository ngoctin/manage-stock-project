const express = require('express');

const router = express.Router();

const statement = require('../app/controller/statement.controller');

router.get('/statement', statement.renderState);

module.exports = router;
