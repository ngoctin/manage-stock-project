const express = require('express');

const router = express.Router();

const supplierController = require('../app/controller/supplier.controller');

router.get('/manage-suppliers', supplierController.renderSuppliers);

router.post('/manage-suppliers', supplierController.insertASupplier);

router.get('/suppliers/search', supplierController.searchAllSuppliers);

module.exports = router;
