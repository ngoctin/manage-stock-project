const express = require('express');

const router = express.Router();

const account = require('../app/controller/account.controller');

router.get('/insert-account', account.renderInsertAccount);

router.post('/insert-account', account.insertAccount);

router.get('/update-accounts', account.renderUpdateAccount);

router.get('/update-password', account.renderUpdatePassword);

router.get('/logout-account', account.logout);

module.exports = router;
