const express = require('express');

const router = express.Router();

const account = require('../app/controller/account.controller');
const authenMiddleware = require('../middlewares/authentication');

router.get('/login-account', authenMiddleware.notRequest, account.renderLogin);

router.post('/login-account', authenMiddleware.notRequest, account.login);

module.exports = router;
