const express = require('express');

const router = express.Router();

const order = require('../app/controller/order.controller');

router.get('/manage-orders', order.renderOrders);

router.get('/order-detail/:id', order.renderOrderDetail);

router.get('/create-order', order.renderCreateOrder);

router.post('/create-oder', order.createOrder);

module.exports = router;
