const express = require('express');

const router = express.Router();

const product = require('../app/controller/product.controller');
const uploadHelper = require('../lib/multerHelper');

/* eslint-disable-next-line no-unused-vars */
router.get('/manage-products', product.renderProducts);

router.get('/delete-product/:id', product.deleteProduct);

router.get('/manage-products/create', product.renderViewAddProduct);

router.post(
  '/manage-products/create',
  uploadHelper.fields([
    {
      name: 'img1Product',
      maxCount: 1,
    },
    {
      name: 'img2Product',
      maxCount: 1,
    },
  ]),
  product.addAProduct
);

router.get('/create-request-bill', product.renderMakeRequestVote);

router.get('/request-bill/create', product.createRequestVote);

router.get('/manage-products/search', product.searchProductsFollowName);

module.exports = router;
