const express = require('express');

const router = express.Router();

const categoryController = require('../../app/controller/category.controller');

router.put('/categories', categoryController.updateACategory);

router.delete('/categories', categoryController.deleteCategory);

module.exports = router;
