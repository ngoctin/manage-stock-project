const express = require('express');

const router = express.Router();

const supplierController = require('../../app/controller/supplier.controller');

router.put('/suppliers', supplierController.updateACategory);

router.delete('/suppliers', supplierController.deleteSupplier);

module.exports = router;
