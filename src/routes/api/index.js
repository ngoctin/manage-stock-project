const express = require('express');

const router = express.Router();
const productRouter = require('./product.router');
const categoryRouter = require('./category.router');
const supplierRouter = require('./supplier.router');

router.use('/', productRouter);
router.use('/', categoryRouter);
router.use('/', supplierRouter);

module.exports = router;
