const express = require('express');

const router = express.Router();
const productController = require('../../app/controller/product.controller');

router.get('/products/instock/:id_product', productController.productInStores);

router.post(
  '/create-request-bill/session/create',
  productController.initializeSessionForMakeVotes
);

router.delete(
  '/create-request-bill/session/delete',
  productController.deleteProductInSession
);

module.exports = router;
