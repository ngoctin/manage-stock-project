const express = require('express');

const router = express.Router();

const employee = require('../app/controller/employee.controller');

router.get('/manage-employees', employee.renderEmployee);

router.get('/delete-employee/:id', employee.deleteEmployee);
module.exports = router;
