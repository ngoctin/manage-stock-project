const express = require('express');

const router = express.Router();

const statistical = require('../app/controller/statistical.controller');

router.get('/manage-statisticals', statistical.renderStatistical);

router.get('/dashboard', statistical.renderStatistical);

module.exports = router;
