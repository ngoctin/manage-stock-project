const express = require('express');

const router = express.Router();

const productRouter = require('./product.router');
const userRouter = require('./user.router');
const employeeRouter = require('./employee.router');
const orderRouter = require('./order.router');
const categoryRouter = require('./category.router');
const supplierRouter = require('./supplier.router');
const customerRouter = require('./customer.router');
const accountRouter = require('./account.router');
const statementRouter = require('./statement.router');
const statisticalRouter = require('./statistial.router');
const noAuthRouter = require('./noAuthentication');

const authenMiddleware = require('../middlewares/authentication');

router.use('/', noAuthRouter);

router.use(
  authenMiddleware.authencated,
  authenMiddleware.getRoleUser,
  authenMiddleware.renderSideBar,
  authenMiddleware.checkRoleOfUser
);

router.use('/', accountRouter);
router.use('/', productRouter);
router.use('/', userRouter);
router.use('/', employeeRouter);
router.use('/', orderRouter);
router.use('/', categoryRouter);
router.use('/', supplierRouter);
router.use('/', customerRouter);
router.use('/', statementRouter);
router.use('/', statisticalRouter);

module.exports = router;
