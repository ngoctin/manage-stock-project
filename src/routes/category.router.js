const express = require('express');

const router = express.Router();

const category = require('../app/controller/category.controller');

router.get('/manage-categories', category.renderCategories);

router.post('/manage-categories', category.insertACategory);

router.get('/manage-categories/search', category.searchCategories);

module.exports = router;
