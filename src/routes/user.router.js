const express = require('express');

const router = express.Router();

const userController = require('../app/controller/user.controller');

router.get('/', userController.renderLogin);
router.get('/login', userController.renderLogin);
router.get('/first-login', userController.renderFirstLogin);
router.get('/error404', userController.renderError404);
router.get('/error500', userController.renderError500);
// router.post('/insert-user', userController.insertUser);
router.get('/settings', userController.renderSettings);
router.get('/settings/change-pass', userController.renderChangePass);
router.get('/logout', userController.renderLogout);
module.exports = router;
