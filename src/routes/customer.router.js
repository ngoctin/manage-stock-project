const express = require('express');

const router = express.Router();

const customer = require('../app/controller/customer.controller');

router.get('/manage-customers', customer.renderCustomers);

module.exports = router;
