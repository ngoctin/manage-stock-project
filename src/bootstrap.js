require('dotenv').config();

const expressConfig = require('./core/express.core');
const logger = require('./lib/logger');

const startServer = async () => {
  try {
    // eslint-disable-next-line
    const app = await expressConfig();

    app.listen(
      process.env.NODE_ENV === 'development' ? process.env.PORT : 7000,
      () => {
        /* eslint-disable-next-line */
        logger.info(
          `[EXPRESS] Server is listening on ${
            process.env.NODE_ENV === 'development' ? process.env.PORT : 7000
          }`
        );
      }
    );
  } catch (error) {
    logger.error(error);
  }
};

startServer();
