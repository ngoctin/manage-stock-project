const bcryptjs = require('bcryptjs');

bcryptjs.hash('123456', 10, function(err, hash) {
  console.log(hash);
});

bcryptjs.compare(
  '123456',
  '$2a$10$Svu8HIMWhIzuiVstF.kRlehV9cIX9MB1.4OO8Bpa20WYcyPheoc8O',
  function(err, res) {
    console.log(res);
  }
);
